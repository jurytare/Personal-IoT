This directory contains device tree and driver for testing linux device driver in rasbian in raspberry pi. Each directory has directories for drivers and devices. Read Makefile inside to know how they work.

In device tree:
	**make** to build dtb from dts
	**make component I=x.dts** to build x.dtb from x.dts
	**make install I=x.dtb** to install device tree to boot directory
	**make remove I=x.dtb** to remove device tree from boot directory

In device driver:
	**make** to build ko module from object in Makefile
	**make install** to insmod ko module to kernel
	**make remove** to rmmod ko module from kernel

Linux header must be installed before building linux modules:
sudo apt-get install raspberrypi-kernel-headers
