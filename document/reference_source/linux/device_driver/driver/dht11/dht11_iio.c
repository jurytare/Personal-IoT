#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
// #include "ioctl_def.h"

#define MAGIC_NUM   'H'

#define SUCCESS 0
#define IO_CMD_READ         _IOR(MAGIC_NUM, 1, int)
#define IO_CMD_WRITE        _IOW(MAGIC_NUM, 2, int)
#define IO_CMD_GPIO_READ      _IOR(MAGIC_NUM, 5, int)
#define IO_CMD_GPIO_WRITE     _IOW(MAGIC_NUM, 6, int)
#define IO_CMD_GET          _IOR(MAGIC_NUM, 7, int)

#define DHT_PIN   20
#define DHT_KICK_START_DELAY 18000
#define DHT_RESP_START  80
#define DHT_DIFF_TIME  100

#define DHT_MAX_BUFFER_SIZE 40
#define HIGH 1
#define LOW 0

struct dht11_t{
  int irq_num;
  unsigned char data[5];
  int time_kick_start; //18ms
  int time_resp_kick; //80us
  int time_diff :8;
  int pin_num   :8;
  int state     :8;
  
//  int i_humidity :8;
//  int d_humidity :8;
//  int i_temperature :8;
//  int d_temperature :8;
//  int checksum  :8;
};

#define DEV_CLASS_NAME "dht"
#define DEVICE_NAME "dht11"

struct dht11_t *dht;
struct cdev *mcdev;
struct class *dev_class;
int major_num, ret;
dev_t dev_num;
long long start_counter;

static irq_handler_t dht_event(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
  long long now = ktime_to_us(ktime_get());
  int index = (dht->state-1) >> 3;
  if(dht->state > 0){
    dht->data[index] <<= 1;
    dht->data[index] |= (now - start_counter > DHT_DIFF_TIME);
  }
//  pr_info("IRQ %d after %lldus, data[%d]=%X\n", 
//      dht->state, now-start_counter, index, ((index < 0)? -1:dht->data[index]));
  start_counter = now;
  dht->state++;
  return (irq_handler_t)IRQ_HANDLED;
}

int device_open(struct inode *inode, struct file *filp)
{
  pr_info("Opened device\n");
  dht = (struct dht11_t*)kmalloc(sizeof(struct dht11_t), GFP_KERNEL);

  dht->pin_num = DHT_PIN;
  dht->time_kick_start = DHT_KICK_START_DELAY;
  dht->time_resp_kick = DHT_RESP_START;
  dht->time_diff = DHT_DIFF_TIME;

  gpio_request(dht->pin_num, "sysfs");
  gpio_direction_input(dht->pin_num);
  gpio_export(dht->pin_num, false);  

  dht->irq_num = gpio_to_irq(dht->pin_num);
  pr_info("DHT11 mapped with IRQn = %d\n", dht->irq_num);

  ret = request_irq(dht->irq_num,
      (irq_handler_t)dht_event,
      IRQF_TRIGGER_RISING,
      "DHT11 interrupt handler",
      NULL);
  disable_irq(dht->irq_num);
  gpio_direction_output(dht->pin_num, HIGH);
  gpio_set_value(dht->pin_num, HIGH);
  if(ret){
    pr_err("Error on create IRQ_handler\n");
  }
  return 0;
}

int device_release(struct inode *inode, struct file *filp)
{
  pr_info("Closed device\n");
  gpio_set_value(dht->pin_num, LOW);
  gpio_direction_input(dht->pin_num);
  gpio_unexport(dht->pin_num);
  gpio_free(dht->pin_num);

  free_irq(dht->irq_num, NULL);
  gpio_unexport(dht->pin_num);
  gpio_free(dht->pin_num);
  kfree(dht);
  return 0;
}
// struct file_operations {
// 	struct module *owner;
// 	loff_t (*llseek) (struct file *, loff_t, int);
// 	ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
// 	ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
// 	ssize_t (*aio_read) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
// 	ssize_t (*aio_write) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
// 	int (*readdir) (struct file *, void *, filldir_t);
// 	unsigned int (*poll) (struct file *, struct poll_table_struct *);
// 	long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
// 	long (*compat_ioctl) (struct file *, unsigned int, unsigned long);
// 	int (*mmap) (struct file *, struct vm_area_struct *);
// 	int (*open) (struct inode *, struct file *);
// 	int (*flush) (struct file *, fl_owner_t id);
// 	int (*release) (struct inode *, struct file *);
// 	int (*fsync) (struct file *, loff_t, loff_t, int datasync);
// 	int (*aio_fsync) (struct kiocb *, int datasync);
// 	int (*fasync) (int, struct file *, int);
// 	int (*lock) (struct file *, int, struct file_lock *);
// 	ssize_t (*sendpage) (struct file *, struct page *, int, size_t, loff_t *, int);
// 	unsigned long (*get_unmapped_area)(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
// 	int (*check_flags)(int);
// 	int (*flock) (struct file *, int, struct file_lock *);
// 	ssize_t (*splice_write)(struct pipe_inode_info *, struct file *, loff_t *, size_t, unsigned int);
// 	ssize_t (*splice_read)(struct file *, loff_t *, struct pipe_inode_info *, size_t, unsigned int);
// 	int (*setlease)(struct file *, long, struct file_lock **);
// 	long (*fallocate)(struct file *file, int mode, loff_t offset, loff_t len);
// };
long device_ioctl(struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param)
{
  pr_notice("Calling ioctl with cmd %d and data %p\n", ioctl_num, (void*)ioctl_param);
  struct Packet{
    void *data;
    int MAX_DATA_SIZE;
    int current_size;
  }temp;
  int ret = copy_from_user(&temp, (void*)ioctl_param, sizeof(struct Packet));
  switch (ioctl_num){
    case IO_CMD_READ:
      //      disable_irq(dht->irq_num);
      //      gpio_direction_output(dht->pin_num, HIGH);
      gpio_set_value(dht->pin_num, LOW);
      msleep(dht->time_kick_start/1000);
      gpio_set_value(dht->pin_num, HIGH);
      udelay(dht->time_resp_kick);
      memset(dht->data, 0, 5);
      dht->state = -1;
      start_counter = ktime_to_us(ktime_get());
      gpio_direction_input(dht->pin_num);
      enable_irq(dht->irq_num);
      msleep(50);
      disable_irq(dht->irq_num);
      gpio_direction_output(dht->pin_num, HIGH);
      gpio_set_value(dht->pin_num, HIGH);
//      pr_notice("Data from DHT11: %X %X %X %X %X\n",
//          dht->data[0], dht->data[1], dht->data[2], dht->data[3], dht->data[4]);
      if(dht->data[4] == (dht->data[1] + dht->data[2] + dht->data[3] + dht->data[0])){
        pr_notice("Data is valid\n");
      }else{
        pr_err("Data got errors\n");
        return -EINVAL;
      }
      break;
    case IO_CMD_GET:
      ret = copy_to_user(temp.data, dht->data, 5);
      temp.current_size = 5 - ret;
//      pr_notice("Data from DHT11: %X %X %X %X %X\n",
//          dht->data[0], dht->data[1], dht->data[2], dht->data[3], dht->data[4]);
      break;
    default:
      return -EINVAL;
  }
  return SUCCESS;
}

struct file_operations fops = {
  .owner = THIS_MODULE,
  .unlocked_ioctl = device_ioctl,
  .open = device_open,
  .release = device_release,
};


static int driver_entry(void)
{
  ret = alloc_chrdev_region(&dev_num, 0, 1, DEVICE_NAME);
  if (ret < 0)
  {
    pr_alert("Fail to allocate a major number\n");
    return ret;
  }
  dev_class = class_create(THIS_MODULE, DEV_CLASS_NAME);
  // major_num = MAJOR(dev_num);
  // pr_info("Major number is %d\n", major_num);
  // pr_info("use \"mknod /dev/%s c %d 0\" for device file\n", DEVICE_NAME, major_num);
  if(!dev_class){
    ret = -1;
    pr_err("Fail to create class %s\n", DEV_CLASS_NAME);
    goto FAIL_ON_DEV_CLASS;
  }
  mcdev = cdev_alloc();
  if(!mcdev){
    pr_err("Cannot create cdev (get NULL)\n");
    goto FAIL_ON_CDEV_ALLOC;
  }
  mcdev->ops = &fops;
  mcdev->owner = THIS_MODULE;

  // int cdev_add(struct cdev *dev, dev_t num, unsigned int count);
  ret = cdev_add(mcdev, dev_num, 1);
  if (ret < 0)
  {
    pr_alert("Unable to add cdev to kernel\n");
    goto FAIL_ON_CDEV_ADD;
  }

  if(IS_ERR(device_create(dev_class,
          NULL, /* no parent device */
          dev_num,
          NULL, /* no additional data */
          DEV_CLASS_NAME))){
    pr_err("Cannot create device class: %s\n",
        DEV_CLASS_NAME);
    goto FAIL_ON_CDEV_ADD;
  };

  pr_notice("%s is ready\n", DEVICE_NAME);
  return 0;
FAIL_ON_CDEV_ADD:
  cdev_del(mcdev);
FAIL_ON_CDEV_ALLOC:
  class_destroy(dev_class);
FAIL_ON_DEV_CLASS:
  unregister_chrdev_region(dev_num, 1);
  return ret;
}

static void driver_exit(void)
{
  device_destroy(dev_class, dev_num);
  cdev_del(mcdev);
  class_unregister(dev_class);
  class_destroy(dev_class);
  unregister_chrdev_region(dev_num, 1);


  pr_alert("Unloaded kernel module\n");
}

module_init(driver_entry);
module_exit(driver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pham Xuan Tra");
