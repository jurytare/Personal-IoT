#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define DEVICE "/dev/dht"

#define MAGIC_NUM   'H'

#define IO_CMD_READ         _IOR(MAGIC_NUM, 1, int)
#define IO_CMD_GET          _IOR(MAGIC_NUM, 7, int)

#define MAX_DATA_SIZE 5

struct Packet{
  void *data;
  int MAX_SIZE;
  int current_size;
};

int main(int argc, char const *argv[])
{
  int ret, fd;
  char ch;
  char buf[MAX_DATA_SIZE];
  double humidity, temperature;
  struct Packet packet = {buf, MAX_DATA_SIZE, 0};

  fd = open(DEVICE, O_RDWR);

  if(fd < 0){
    printf("file %s does not exist or has been locked by another process", DEVICE);
    exit(-1);
  }
  while(1){
    printf(""
        "q to exit\n"
        "1 to Querry\n"
        "2 to Get\n"
        "Enter command: "
        );
    scanf(" %c", &ch);
    switch(ch){
      case 'q':
        close(fd);
        return 0;
      case '1':
        printf("IOCTL test read (%d) result: %d\n", IO_CMD_READ, ioctl(fd, IO_CMD_READ, 0));
        break;
      case '2':
        printf("IOCTL test write (%d) result: %d\n", IO_CMD_GET, ioctl(fd, IO_CMD_GET, &packet));
        printf("DHT11 data: %x %x %x %x\n", 
            buf[0], buf[1], buf[2], buf[3]);
        humidity = (double)buf[0] + (buf[1]? 1.0/(double)buf[1]:0);
        temperature = (double)buf[2] + (buf[3]? 1.0/(double)buf[3]:0);
        printf("Humidity = %.3f%%\n", humidity);
        printf("Temperature = %.3f C\n", temperature);
        break;
      default:
        break;
    }
  }
  return 0;
}
