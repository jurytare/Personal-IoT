#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>

// #include "ioctl_def.h"

#define MAGIC_NUM   'T'

#define SUCCESS 0
#define IO_CMD_READ         _IOR(MAGIC_NUM, 1, int)
#define IO_CMD_WRITE        _IOW(MAGIC_NUM, 2, int)
#define IO_CMD_GPIO_READ      _IOR(MAGIC_NUM, 5, int)
#define IO_CMD_GPIO_WRITE     _IOW(MAGIC_NUM, 6, int)

#define LED_PIN   21
#define BUTTON_PIN 20
#define HIGH 1
#define LOW 0

static int gpio_irq_number;

#define MAX_SIZE 100
#define DEV_CLASS_NAME "my_device"


struct fake_device
{
  char data[MAX_SIZE];
  int current_size;
  struct semaphore sem;
} virtual_device;

struct cdev *mcdev;
struct class *dev_class;
int major_num, ret;
dev_t dev_num;

#define DEVICE_NAME "my_device"

static irq_handler_t button_event(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
  pr_notice("%s: Got IRQ=%d\n", __func__, irq);
  return (irq_handler_t)IRQ_HANDLED;
}

int device_open(struct inode *inode, struct file *filp)
{
  if (down_interruptible(&virtual_device.sem) != 0)
  {
    pr_alert("could not lock device during open\n");
    return -1;
  }

  pr_info("Opened device\n");
  return 0;
}

ssize_t device_read(struct file *filp, char *bufStoreData, size_t bufCount, loff_t *curOffset)
{
  if (bufCount > virtual_device.current_size)
  {
    pr_alert("Request %d! Driver will return %d", (int)bufCount, MAX_SIZE);
    bufCount = virtual_device.current_size;
  }
  pr_info("Reading %d bytes from device\n", (int)bufCount);
  ret = copy_to_user(bufStoreData, virtual_device.data, bufCount);
  pr_info("Finish reading from device\n");
  virtual_device.current_size -= bufCount;
  return bufCount - ret;
}

ssize_t device_write(struct file *filp, const char *bufStoreData, size_t bufCount, loff_t *curOffset)
{
  if (bufCount > MAX_SIZE)
  {
    pr_alert("Request %d! Driver will record %d", (int)bufCount, MAX_SIZE);
    bufCount = MAX_SIZE;
  }
  pr_info("Writing %d byte(s) to device\n", (int)bufCount);
  ret = copy_from_user(virtual_device.data, bufStoreData, bufCount);
  pr_info("Finish writing from device\n");
  virtual_device.current_size = bufCount;
  return ret;
}

int device_release(struct inode *inode, struct file *filp)
{
  up(&virtual_device.sem);
  pr_info("Closed device\n");
  return 0;
}
// struct file_operations {
// 	struct module *owner;
// 	loff_t (*llseek) (struct file *, loff_t, int);
// 	ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
// 	ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
// 	ssize_t (*aio_read) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
// 	ssize_t (*aio_write) (struct kiocb *, const struct iovec *, unsigned long, loff_t);
// 	int (*readdir) (struct file *, void *, filldir_t);
// 	unsigned int (*poll) (struct file *, struct poll_table_struct *);
// 	long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
// 	long (*compat_ioctl) (struct file *, unsigned int, unsigned long);
// 	int (*mmap) (struct file *, struct vm_area_struct *);
// 	int (*open) (struct inode *, struct file *);
// 	int (*flush) (struct file *, fl_owner_t id);
// 	int (*release) (struct inode *, struct file *);
// 	int (*fsync) (struct file *, loff_t, loff_t, int datasync);
// 	int (*aio_fsync) (struct kiocb *, int datasync);
// 	int (*fasync) (int, struct file *, int);
// 	int (*lock) (struct file *, int, struct file_lock *);
// 	ssize_t (*sendpage) (struct file *, struct page *, int, size_t, loff_t *, int);
// 	unsigned long (*get_unmapped_area)(struct file *, unsigned long, unsigned long, unsigned long, unsigned long);
// 	int (*check_flags)(int);
// 	int (*flock) (struct file *, int, struct file_lock *);
// 	ssize_t (*splice_write)(struct pipe_inode_info *, struct file *, loff_t *, size_t, unsigned int);
// 	ssize_t (*splice_read)(struct file *, loff_t *, struct pipe_inode_info *, size_t, unsigned int);
// 	int (*setlease)(struct file *, long, struct file_lock **);
// 	long (*fallocate)(struct file *file, int mode, loff_t offset, loff_t len);
// };
long device_ioctl(struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param)
{
  pr_notice("Calling ioctl with cmd %d and data %p\n", ioctl_num, (void*)ioctl_param);
  struct Packet{
    void *data;
    int MAX_DATA_SIZE;
    int current_size;
  }temp;
  copy_from_user(&temp, (void*)ioctl_param, sizeof(struct Packet));
  switch (ioctl_num){
    case IO_CMD_READ:
      if(temp.current_size > virtual_device.current_size){
        temp.current_size = virtual_device.current_size;
      }
      pr_info("User require reading %d bytes from device\n", temp.current_size);
      copy_to_user(temp.data, virtual_device.data, temp.current_size);
      copy_to_user((void*)ioctl_param, &temp, sizeof(struct Packet));
      virtual_device.current_size -= temp.current_size;
      break;
    case IO_CMD_WRITE:
      if(temp.current_size > MAX_SIZE){
        temp.current_size = MAX_SIZE;
      }
      copy_from_user(virtual_device.data, temp.data, temp.current_size);
      pr_info("User require writing %d bytes to device\n", temp.current_size);
      virtual_device.current_size = temp.current_size;
      break;
    case IO_CMD_GPIO_READ:
      pr_notice("Got IO_CMD_GPIO_READ\n");
      int button = gpio_get_value(BUTTON_PIN);
      copy_to_user((void*)ioctl_param, &button, sizeof(int));
      break;
    case IO_CMD_GPIO_WRITE:
      pr_notice("Got IO_CMD_GPIO_WRITE\n");
      int LEDstate;
      copy_from_user(&LEDstate, (void*)ioctl_param, sizeof(int));
      gpio_set_value(LED_PIN, LEDstate);
      pr_notice("Switch LED output to %s\n", LEDstate? "HIGH":"LOW");
      break;
    default:
      return -EINVAL;
  }
  return SUCCESS;
}

struct file_operations fops = {
  .owner = THIS_MODULE,
  .llseek = NULL,
  .read = device_read,
  .write = device_write,
  .unlocked_ioctl = device_ioctl,
  .open = device_open,
  .release = device_release,
};

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pham Xuan Tra");

static int driver_entry(void)
{
  ret = alloc_chrdev_region(&dev_num, 0, 1, DEVICE_NAME);
  if (ret < 0)
  {
    pr_alert("Fail to allocate a major number\n");
    return ret;
  }
  dev_class = class_create(THIS_MODULE, DEV_CLASS_NAME);
  // major_num = MAJOR(dev_num);
  // pr_info("Major number is %d\n", major_num);
  // pr_info("use \"mknod /dev/%s c %d 0\" for device file\n", DEVICE_NAME, major_num);
  if(!dev_class){
    ret = -1;
    pr_err("Fail to create class %s\n", DEV_CLASS_NAME);
    goto FAIL_ON_DEV_CLASS;
  }
  mcdev = cdev_alloc();
  if(!mcdev){
    pr_err("Cannot create cdev (get NULL)\n");
    goto FAIL_ON_CDEV_ALLOC;
  }
  mcdev->ops = &fops;
  mcdev->owner = THIS_MODULE;

  // int cdev_add(struct cdev *dev, dev_t num, unsigned int count);
  ret = cdev_add(mcdev, dev_num, 1);
  if (ret < 0)
  {
    pr_alert("Unable to add cdev to kernel\n");
    goto FAIL_ON_CDEV_ADD;
  }

  if(IS_ERR(device_create(dev_class,
          NULL, /* no parent device */
          dev_num,
          NULL, /* no additional data */
          DEV_CLASS_NAME))){
    pr_err("Cannot create device class: %s\n",
        DEV_CLASS_NAME);
    goto FAIL_ON_CDEV_ADD;
  };

  sema_init(&virtual_device.sem, 1);
  virtual_device.current_size = 0;

  gpio_request(LED_PIN, "sysfs");
  gpio_direction_output(LED_PIN, LOW);
  gpio_export(LED_PIN, false);

  gpio_request(BUTTON_PIN, "sysfs");
  gpio_direction_input(BUTTON_PIN);
  gpio_set_debounce(BUTTON_PIN, 500);
  gpio_export(BUTTON_PIN, false);

  pr_info("Start module with button state: %s\n",
      gpio_get_value(BUTTON_PIN));
  gpio_irq_number = gpio_to_irq(BUTTON_PIN);
  pr_info("Button mapped with IRQn = %d\n",gpio_irq_number);

  ret = request_irq(gpio_irq_number,
        (irq_handler_t)button_event,
        IRQF_TRIGGER_RISING,
        "Button interrupt handler",
        NULL);
  if(ret){
    pr_err("Error on create IRQ_handler\n");
  }

  return 0;
FAIL_ON_CDEV_ADD:
  cdev_del(mcdev);
FAIL_ON_CDEV_ALLOC:
  class_destroy(dev_class);
FAIL_ON_DEV_CLASS:
  unregister_chrdev_region(dev_num, 1);
  return ret;
}

static void driver_exit(void)
{
  device_destroy(dev_class, dev_num);
  cdev_del(mcdev);
  class_unregister(dev_class);
  class_destroy(dev_class);
  unregister_chrdev_region(dev_num, 1);

  gpio_set_value(LED_PIN, LOW);
  gpio_unexport(LED_PIN);
  gpio_free(LED_PIN);

  free_irq(gpio_irq_number, NULL);
  gpio_unexport(BUTTON_PIN);
  gpio_free(BUTTON_PIN);
  

  pr_alert("Unloaded kernel module\n");
}

module_init(driver_entry);
module_exit(driver_exit);
