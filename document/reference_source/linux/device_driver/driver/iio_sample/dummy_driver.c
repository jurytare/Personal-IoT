#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>      /* For platform devices */
#include <linux/interrupt.h>            /* For IRQ */
#include <linux/of.h>                   /* For DT*/
#include <linux/iio/iio.h>    /* mandatory */
#include <linux/iio/sysfs.h>  /* mandatory since sysfs are used */
#include <linux/iio/events.h> /* For advanced users, to manage iio events */
#include <linux/iio/buffer.h>  /* mandatory to use triggered buffers */

#define DEVICE_NAME "dummy_device"

#define FAKE_VOLTAGE_CHANNEL(num)   \
{                               \
  .type = IIO_VOLTAGE,        \
  .indexed = 1,               \
  .channel = (num),           \
  .address = (num),           \
  .info_mask_separate = BIT(IIO_CHAN_INFO_RAW),           \
  .info_mask_shared_by_type = BIT(IIO_CHAN_INFO_SCALE)    \
}

struct my_private_data {
  int foo;
  int bar;
  struct mutex lock;
};

#define DEV_CLASS_NAME "dummy_device"

struct cdev *mcdev;
struct class *dev_class;
int major_num, ret;
dev_t dev_num;
static int character_dev_begin(void);
static void character_dev_exit(void);
long device_ioctl(struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param);
int device_release(struct inode *inode, struct file *filp);
int device_open(struct inode *inode, struct file *filp);
ssize_t read_char (struct file *filp, char __user * data, size_t size, loff_t *offset);
ssize_t write_char (struct file *filp, const char __user *data, size_t size, loff_t *offset);


static int fake_read_raw(struct iio_dev *indio_dev,
    struct iio_chan_spec const *channel, int *val,
    int *val2, long mask)
{
  printk(KERN_INFO "%s is called\n", __func__);
  *val = 100;
  *val2 = 300;
  return 0;
}

static int fake_write_raw(struct iio_dev *indio_dev,
    struct iio_chan_spec const *chan,
    int val, int val2, long mask)
{
  printk(KERN_INFO "%s is called\n", __func__);
  pr_notice("Parameter: val=%d, val2=%d, mask=%ld\n", val, val2, mask);
  return 0;
}

static const struct iio_chan_spec fake_channels[] = {
  FAKE_VOLTAGE_CHANNEL(0),
  FAKE_VOLTAGE_CHANNEL(1),
  FAKE_VOLTAGE_CHANNEL(2),
  FAKE_VOLTAGE_CHANNEL(3),
};

static const struct of_device_id iio_dummy_ids[] = {
  { .compatible = "packt,"DEVICE_NAME, },
  { /* sentinel */ }
};

static const struct iio_info fake_iio_info = {
  .read_raw = fake_read_raw,
  .write_raw    = fake_write_raw,
  .driver_module = THIS_MODULE,
};

static int my_pdrv_probe (struct platform_device *pdev)
{
  struct iio_dev *indio_dev;
  struct my_private_data *data;
  pr_notice("Got a device: %s\n", pdev->name);

  indio_dev = devm_iio_device_alloc(&pdev->dev, sizeof(*data));
  if (!indio_dev) {
    dev_err(&pdev->dev, "iio allocation failed!\n");
    return -ENOMEM;
  }

  data = iio_priv(indio_dev);
  mutex_init(&data->lock);
  indio_dev->dev.parent = &pdev->dev;
  indio_dev->info = &fake_iio_info;
  indio_dev->name = KBUILD_MODNAME;
  indio_dev->modes = INDIO_DIRECT_MODE;
  indio_dev->channels = fake_channels;
  indio_dev->num_channels = ARRAY_SIZE(fake_channels);
  indio_dev->available_scan_masks = 0xF;

  int ret = character_dev_begin();
  if(ret){
    pr_err("Something wrong in character device\n");
  }else{
    pr_notice("Character device is ready\n");
    indio_dev->chrdev = *mcdev;
  }

  iio_device_register(indio_dev);

  platform_set_drvdata(pdev, indio_dev);
  return 0;
}

static int my_pdrv_remove(struct platform_device *pdev)
{
  pr_notice("End of %s\n", pdev->name);
  character_dev_exit();
  struct iio_dev *indio_dev = platform_get_drvdata(pdev);
  iio_device_unregister(indio_dev);
  iio_device_free(indio_dev);
  return 0;
}

static struct platform_driver mypdrv = {
  .probe      = my_pdrv_probe,
  .remove     = my_pdrv_remove,
  .driver     = {
    .name     = DEVICE_NAME,
    .of_match_table = of_match_ptr(iio_dummy_ids),  
    .owner    = THIS_MODULE,
  },
};

module_platform_driver(mypdrv);
MODULE_DEVICE_TABLE(of, iio_dummy_ids);
MODULE_AUTHOR("John Madieu <john.madieu@gmail.com>");
MODULE_LICENSE("GPL");

struct file_operations fops = {
  .owner = THIS_MODULE,
  // .unlocked_ioctl = device_ioctl,
  .read = read_char,
  .write = write_char,
  .open = device_open,
  .release = device_release,
};


static int character_dev_begin(void)
{
  ret = alloc_chrdev_region(&dev_num, 0, 1, DEVICE_NAME);
  if (ret < 0)
  {
    pr_alert("Fail to allocate a major number\n");
    return ret;
  }
  dev_class = class_create(THIS_MODULE, DEV_CLASS_NAME);
  // major_num = MAJOR(dev_num);
  // pr_info("Major number is %d\n", major_num);
  // pr_info("use \"mknod /dev/%s c %d 0\" for device file\n", DEVICE_NAME, major_num);
  if(!dev_class){
    ret = -1;
    pr_err("Fail to create class %s\n", DEV_CLASS_NAME);
    goto FAIL_ON_DEV_CLASS;
  }
  mcdev = cdev_alloc();
  if(!mcdev){
    pr_err("Cannot create cdev (get NULL)\n");
    goto FAIL_ON_CDEV_ALLOC;
  }
  mcdev->ops = &fops;
  mcdev->owner = THIS_MODULE;

  // int cdev_add(struct cdev *dev, dev_t num, unsigned int count);
  ret = cdev_add(mcdev, dev_num, 1);
  if (ret < 0)
  {
    pr_alert("Unable to add cdev to kernel\n");
    goto FAIL_ON_CDEV_ADD;
  }

  if(IS_ERR(device_create(dev_class,
          NULL, /* no parent device */
          dev_num,
          NULL, /* no additional data */
          DEV_CLASS_NAME))){
    pr_err("Cannot create device class: %s\n",
        DEV_CLASS_NAME);
    goto FAIL_ON_CDEV_ADD;
  };

  pr_notice("%s is ready\n", DEVICE_NAME);
  return 0;
FAIL_ON_CDEV_ADD:
  cdev_del(mcdev);
FAIL_ON_CDEV_ALLOC:
  class_destroy(dev_class);
FAIL_ON_DEV_CLASS:
  unregister_chrdev_region(dev_num, 1);
  return ret;
}

static void character_dev_exit(void)
{
  device_destroy(dev_class, dev_num);
  cdev_del(mcdev);
  class_unregister(dev_class);
  class_destroy(dev_class);
  unregister_chrdev_region(dev_num, 1);


  pr_alert("Unloaded kernel module\n");
}

ssize_t read_char (struct file *filp, char __user * data, size_t size, loff_t *offset)
{
  pr_notice("%s: was called\n", __func__);
  return 0;
} 

 ssize_t write_char (struct file *filp, const char __user *data, size_t size, loff_t *offset){
  pr_notice("%s: was called\n", __func__);
  return 0;
}

int device_release(struct inode *inode, struct file *filp)
{
  pr_info("Closed device\n");
  return 0;
}

int device_open(struct inode *inode, struct file *filp)
{
  pr_info("Opened device\n");
  return 0;
}