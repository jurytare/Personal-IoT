#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define DEVICE "/dev/my_device"

#define MAGIC_NUM   'T'

#define IO_CMD_READ         _IOR(MAGIC_NUM, 1, int)
#define IO_CMD_WRITE        _IOW(MAGIC_NUM, 2, int)
#define IO_CMD_TIME_ON      _IO(MAGIC_NUM, 3)
#define IO_CMD_TIME_OFF     _IO(MAGIC_NUM, 4)
#define IO_CMD_GPIO_READ    _IOR(MAGIC_NUM, 5, int)
#define IO_CMD_GPIO_WRITE   _IOW(MAGIC_NUM, 6, int)
#define IO_CMD_GET          _IOR(MAGIC_NUM, 7, int)

struct Packet{
  void *data;
  int MAX_SIZE;
  int current_size;
};

static const int MAX_DATA_SIZE = 200;

int main(int argc, char const *argv[])
{
  int ret, fd;
  char ch, write_buf[MAX_DATA_SIZE], read_buf[MAX_DATA_SIZE];
  struct Packet rd_packet = {read_buf, MAX_DATA_SIZE, 0};
  struct Packet wr_packet = {write_buf, MAX_DATA_SIZE, 0};

  fd = open(DEVICE, O_RDWR);

  if(fd < 0){
    printf("file %s does not exist or has been locked by another process", DEVICE);
    exit(-1);
  }
  while(1){
    printf(""
        "r = read from device\n"
        "w = write to device\n"
        "q to exit\n"
        "1 to Querry\n"
        "2 to Get data\n"
        "Enter command: "
        );
    scanf(" %c", &ch);
    unsigned char *temp;
    switch(ch){
      case 'q':
        close(fd);
        return 0;
      case '1':
        printf("IOCTL test read (%d) result: %d\n", IO_CMD_READ, ioctl(fd, IO_CMD_GPIO_READ, &rd_packet));
        break;
      case '2':
        printf("IOCTL test write (%d) result: %d\n", IO_CMD_WRITE, ioctl(fd, IO_CMD_GPIO_WRITE, &wr_packet));
        temp = wr_packet.data;
        printf("Data recorded: %x %x %x %x\n",
            temp[1], temp[2], temp[3], temp[4]);
        break;
      case '3':
        printf("IOCTL test time on result: %d\n", ioctl(fd, IO_CMD_TIME_ON, 0));
        break;
      case '4':
        printf("IOCTL test time off result: %d\n", ioctl(fd, IO_CMD_TIME_OFF, 0));
      case '5':
        printf("Enter gpio value: ");scanf(" %d", &ret);
        printf("IOCTL test gpio write (%d) result: %d\n", IO_CMD_GPIO_WRITE, ioctl(fd, IO_CMD_GPIO_WRITE, &ret));
        ret = 0;
        break;
      case '6':
        printf("IOCTL test gpio read (%d) result: %d\n", IO_CMD_GPIO_READ, ioctl(fd, IO_CMD_GPIO_READ, &ret));
        printf("Current state of button: %s\n", ret? "HIGH":"LOW");
        ret = 0;
        break;
      default:
        break;
    }
  }
  return 0;
}
