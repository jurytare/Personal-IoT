#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/i2c.h>

#define MAGIC_NUM   'P'

#define SUCCESS 0
#define IO_CMD_READ         _IOR(MAGIC_NUM, 1, int)
#define IO_CMD_WRITE        _IOW(MAGIC_NUM, 2, int)
#define IO_CMD_GPIO_READ      _IOR(MAGIC_NUM, 5, int)
#define IO_CMD_GPIO_WRITE     _IOW(MAGIC_NUM, 6, int)
#define IO_CMD_GET          _IOR(MAGIC_NUM, 7, int)

#define HIGH 1
#define LOW 0


#define DRIVER_NAME "gpower"

static int gpower_probe(struct i2c_client *client, \
  const struct i2c_device_id *id)
{
  pr_notice("Hello! %s for %s works\n", \
    __func__, client->name);
  return 0;
}

static int gpower_remove(struct i2c_client *client)
{
  pr_notice("Good bye! %s for %s\n", \
    __func__, client->name);
  return 0;
}

static struct i2c_device_id gpower_idtable[] = {
    { "msp-power", 0001 },
    { "esp-power", 0002 },
    { }
};
MODULE_DEVICE_TABLE(i2c, gpower_idtable);
static struct i2c_driver gpower_driver = {
  .driver = { 
    .owner = THIS_MODULE,
    .name = DRIVER_NAME,
  },
  .probe  = gpower_probe,
  .remove = gpower_remove,
  .id_table = gpower_idtable,
};

module_i2c_driver(gpower_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pham Xuan Tra");
