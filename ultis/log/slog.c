#include "slog.h"
#include <stdio.h>
#include <stdarg.h>
#include <pthread.h>

const char* SLOG_LEVEL_TAG[] = {
	"DEBUG",
	"INFO",
	"WARNING",
	"ERROR",
	"FATAL"
};

static int log_filter;
static FILE *log_file = NULL;
static pthread_mutex_t log_mt = PTHREAD_MUTEX_INITIALIZER;

int slog_init(const char *address, int mode, int filter)
{
	log_filter = filter;
	if(!mode){
		if(address){
			log_file = fopen(address, "w");
		}else{
			return -1;
		}
	}else{
		log_file = stdout;
	}
	if(log_file){
		fprintf(log_file, "\n--STARTING LOGGING--\n");
		fflush(log_file);
		return 0;
	}
	return -2;
}

int slog_deinit()
{
	fflush(log_file);
	if(log_file){
		fclose(log_file);
	}
	return 0;
}

int slog(enum slog_level level, const char* file, \
		const char* func, int line, const char* content, ...)
{
	if((1 << level) & log_filter){
		char buf[256] = {0};
		int ret;
		ret = snprintf(buf, 256, "%s:%s:%s:%d: %s\n", \
				SLOG_LEVEL_TAG[level], file, func, line, content);

		va_list args;
		va_start (args, content);
		if(pthread_mutex_lock(&log_mt)){
			fprintf(stderr, "ERROR: Unable to lock the log\n");
			return -1;
		}
		if(log_file){
			vfprintf(log_file, buf, args);
			fflush(log_file);
		}else{
			vprintf(buf, args);
		}
		if(pthread_mutex_unlock(&log_mt)){
			return -1;
			fprintf(stderr, "ERROR: Unable to release the log\n");
		}
		va_end (args);
		return 0;
	}else{
		return 1;
	}
}
