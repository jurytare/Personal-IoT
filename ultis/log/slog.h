#ifndef __SHARE_LOG__H__
#define __SHARE_LOG__H__

enum slog_level{
	slog_debug = 0,
	slog_info,
	slog_warn,
	slog_err,
	slog_fatal
};

enum slog_flag_level{
	slog_debug_flag   = 1 << slog_debug,
	slog_info_flag    = 1 << slog_info,
	slog_warn_flag    = 1 << slog_warn,
	slog_err_flag     = 1 << slog_err,
	slog_fatal_flag   = 1 << slog_fatal,
  slog_all_flag     = 0xFF
};

#define _SLOG_(x,y, ...) slog(x, __FILE__, __func__, __LINE__, y, ##__VA_ARGS__)

#define SLOG_D(y,...) _SLOG_(slog_debug,y,##__VA_ARGS__)
#define SLOG_I(y,...) _SLOG_(slog_info,y,##__VA_ARGS__)
#define SLOG_W(y,...) _SLOG_(slog_warn,y,##__VA_ARGS__)
#define SLOG_E(y,...) _SLOG_(slog_err,y,##__VA_ARGS__)
#define SLOG_F(y,...) _SLOG_(slog_fatal,y,##__VA_ARGS__)

int slog_init(const char *address, int mode, int filter);
int slog_deinit();
int slog(enum slog_level level, const char* file, const char* func, int line, const char* content, ...);

#endif
