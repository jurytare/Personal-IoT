# meta-osGateway

Embeded layer in Yocto Linux OS for Multi-device connectivity in IoT World

Concept: As a gateway of connectivity of all devices, the Os (Linux platform) will see every device as a device file which has specific driver to handle

Structure:
  - Poky (Yocto project for Linux OS)
  - meta-raspberrypi (for creating OS which is capable in Raspberry pi)
  - meta-osGateway (for creating feature for Linux OS)

Installation:
  - When your Yocto source are available, just clone the source and add features to local.conf
  - Or clone the source and run the setupSource.sh for the rest of installation
