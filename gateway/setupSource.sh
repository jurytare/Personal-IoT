#!/bin/bash
if [ "$1" == "" ]; then
  echo 'Please run with the mode:'
  echo '1: when you are in meta-osgateway directory'
  echo '2: when you do not have meta-osgateway yet'
  exit -1
fi

if [ "$1" == "1" ]; then
  cd ..
fi

git clone git://git.yoctoproject.org/poky

if [ "$1" == "1" ]; then
  mv meta-osgateway poky/.
  cd poky
else
  cd poky
  git clone https://gitlab.com/jurytare/meta-osgateway.git
fi

git clone git://git.yoctoproject.org/meta-raspberrypi
ADDR_BASE=$(pwd)

source oe-init-build-env rpi_build

echo "

MACHINE ?= \"raspberrypi3\" 
ENABLE_UART = \"1\" 
CORE_IMAGE_EXTRA_INSTALL += \"openssh\" 
" >> conf/local.conf

echo '# POKY_BBLAYERS_CONF_VERSION is increased each time build/conf/bblayers.conf
# changes incompatibly
POKY_BBLAYERS_CONF_VERSION = "2"

BBPATH = "${TOPDIR}"
BBFILES ?= ""
' > conf/bblayers.conf

echo "BBLAYERS ?= \" \\
  $ADDR_BASE/meta \\
  $ADDR_BASE/meta-poky \\
  $ADDR_BASE/meta-yocto-bsp \\
  $ADDR_BASE/meta-raspberrypi \\
  $ADDR_BASE/meta-osgateway \\
  \"
BBLAYERS_NON_REMOVABLE ?= \" \\
  $ADDR_BASE/meta \\
  $ADDR_BASE/meta-poky \\
  \"
" >> conf/bblayers.conf

echo "
Source code are ready for using
Go to poky folder and execute: source oe-init-build-env rpi_build
Then start build your image by: bitbake core-image-base"

