#include "node_link.h"
#include <stdint.h>
#include <string.h>
#include "slog.h"

//static const char* HARDWARE_ID = "0001";

static int link_switch_rx2tx(struct LinkNode_t *ln)
{
  SLOG_D("");
  simSPI_setMode(&ln->PHY_handler, simSPI_mode_TX);
  return 0;
}

static int link_switch_tx2rx(struct LinkNode_t *ln)
{
  SLOG_D("");
  simSPI_clearMode(&ln->PHY_handler, simSPI_mode_TX);
  return 0;
}

static int link_on_event_send(struct LinkNode_t *ln, unsigned char *sendIt)
{
  int ret;
  ret = 0;

  if(ln->rcv.status < e_link_data){
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status]);
  }else if( e_link_crc_2 > ln->rcv.status){
    SLOG_I("%d %s %c", ln->rcv.status, link_status_text[e_link_data], *ln->p_rec);
  }else{
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status - e_link_crc_2 + e_link_data + 1]);
  }
  
  switch(ln->rcv.status){
    case e_link_size:
      *sendIt = ln->tx_size;
      ln->rcv.status = e_link_data;
      ln->p_rec = ln->tx_buf;
    break;
    case e_link_crc_2:
      link_switch_tx2rx(ln);
    case e_link_feedback:
      *sendIt = ln->rcv.crc ^ CRC_IDENTITY;
      SLOG_D("Feedback CRC = %d", *sendIt);
      ln->rcv.status = e_link_finish;
      LinkNode_stop_session(ln);
    break;
    default:
      if(ln->rcv.status - e_link_data == ln->tx_size){
        SLOG_D("Complete sending");
        ln->rcv.status = e_link_crc_2;
        *sendIt = ln->rcv.crc;
      }else if(ln->rcv.status >= e_link_finish){
        LinkNode_stop_session(ln);
        ret = -2;
      }else{
        SLOG_D("Send %d", *ln->p_rec); 
        *sendIt = *ln->p_rec++;
        ln->rcv.status++;
        ln->rcv.crc ^= *sendIt;
      }
  }
  return ret;
}

static int link_on_event_rcv(struct LinkNode_t *ln, unsigned char data)
{
  if(ln->rcv.status < e_link_data){
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status]);
  }else if( e_link_crc_2 > ln->rcv.status){
    SLOG_I("%d %s %c", ln->rcv.status, link_status_text[e_link_data], data);
  }else{
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status - e_link_crc_2 + e_link_data + 1]);
  }

  switch(ln->rcv.status){
    case e_link_crc_1:

      if((ln->rcv.crc ^ data) == CRC_IDENTITY){
        ln->rcv.crc = 0;
        /* Check address of the package if this address is for this node or not */
        if(ln->rcv.addr == ln->soft_ID){
          SLOG_D("Processing header %d", ln->rcv.cmd);
          switch(ln->rcv.cmd){
            case e_link_cmd_h2n:
        ln->rcv.status = e_link_size;
        ln->p_rec = &ln->rcv.size;
            break;
            case e_link_cmd_n2h:
              ln->rcv.status = e_link_size;
              link_switch_rx2tx(ln);
            break;
            case e_link_cmd_ping:
              ln->rcv.status = e_link_feedback;
              link_switch_rx2tx(ln);
            break;
            default:
              SLOG_W("Unknown command");
            break;
          }
          return 0;
        }else{
          SLOG_W("Different address: %d", ln->rcv.addr);
        }
      }else{
        /* Failed to receive header */
        SLOG_E("Failed to detect CRC_0: %d & %d", data, ln->rcv.crc);
        LinkNode_stop_session(ln);
        /* Should have a counter for failures.
         * If it fails too much, consider to reset.
         */
      }
    break;
    case e_link_feedback:
      ln->rcv.status = e_link_finish + ((ln->rcv.crc ^ data) != CRC_IDENTITY);
      SLOG_I("End of transaction with result %d (CRC:%d)", ln->rcv.status, ln->rcv.crc);
      LinkNode_stop_session(ln);
    break;
    default:
      if(ln->rcv.status - e_link_data == ln->rcv.size){
        SLOG_D("Complete recording. Check CRC_2 now");
        if(ln->rcv.crc == data){
          ln->rcv.status = e_link_feedback;
          ln->p_rec = &ln->rcv.crc;
          link_switch_rx2tx(ln);
        }
      }else if(ln->rcv.status < sizeof(struct LinkLayer_package)){
        SLOG_D("Record: Data=0x%.2x, status=%d, crc=0x%.2x", data, ln->rcv.status, ln->rcv.crc);
        *ln->p_rec++ = data;
        ln->rcv.status++;
        ln->rcv.crc ^= data;
      }else{
        ln->rcv.status = e_link_error;
        LinkNode_stop_session(ln);
      }
    break;
  }

  return 0;
}

static int link_layer_events(struct simSPI_t *ss)
{
  SLOG_D("");
  if ((ss->mode && BIT(simSPI_mode_start_isr)) \
      && (ss->status & BIT(simSPI_status_hasNotif))){
    LinkNode_start_session((struct LinkNode_t*)ss);
  }

  unsigned char ch;
  if (ss->mode & BIT(simSPI_mode_TX)){
    if(!(ss->status & BIT(simSPI_status_TxIsBusy))){
      if(!link_on_event_send((struct LinkNode_t*)ss, &ch)){
        simSPI_data_send(ss, ch);
      }
    }
  }else{
    if(ss->status & BIT(simSPI_status_RxHasData)){
      simSPI_data_get(ss, &ch);
      link_on_event_rcv((struct LinkNode_t*)ss, ch);
    }
  }
}

int LinkNode_init(struct LinkNode_t *ln)
{
  int ret;
  ln->rcv.size = ln->tx_size = 0;
  ln->tx_buf = NULL;
  ln->soft_ID = 32;
  /* For specific PHY layer*/
  ret = simSPI_init(&ln->PHY_handler, 0);
  if(ret){
    return ret;
  }
  simSPI_register_isr(&ln->PHY_handler, simSPI_mode_start_isr);
  simSPI_set_isr_callback(&ln->PHY_handler, &link_layer_events);
  LinkNode_start_session(ln);
  return 0;
}

int LinkNode_deinit(struct LinkNode_t *ln)
{
  SLOG_D("");
  simSPI_deinit(&ln->PHY_handler);
  return 0;
}

int LinkNode_start_session(struct LinkNode_t *ln)
{
  SLOG_D("");
  uint8_t temp;
  ln->p_rec = (uint8_t *)&ln->rcv;
  ln->rcv.status = ln->rcv.crc = 0;
  ln->rcv.size = MAX_DATA_FRAME;
  simSPI_register_isr(&ln->PHY_handler, simSPI_mode_TRX_isr);
  simSPI_data_get(&ln->PHY_handler, &temp);
  link_switch_tx2rx(ln);
  simSPI_start(&ln->PHY_handler);
}

int LinkNode_stop_session(struct LinkNode_t *ln)
{
  SLOG_D("Node stop transaction");
    simSPI_unregister_isr(&ln->PHY_handler, simSPI_mode_TRX_isr);
}

int LinkNode_send_package(struct LinkNode_t *ln, void* data, unsigned char data_size)
{
  ln->tx_size = data_size;
  ln->tx_buf = data;
    SLOG_I("Start sending %s, length =  %d", data, data_size);
 
  while(ln->rcv.status < e_link_finish);
  return (e_link_finish == ln->rcv.status);
}

int LinkNode_get_package(struct LinkNode_t *ln, void* data, unsigned char data_size)
{
  while(e_link_finish != ln->rcv.status);
  unsigned char data_cpy = data_size < ln->rcv.size? \
      data_size : ln->rcv.size;
  memcpy(data, ln->rcv.buf, data_cpy);
  ln->rcv.status = e_link_idle;
  return data_cpy;
}