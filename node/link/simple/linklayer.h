#ifndef __LINK_LAYER_H_
#define __LINK_LAYER_H_

#define MAX_DATA_FRAME 32
#define CRC_IDENTITY 0xAA

enum e_link_status{
	e_link_address = 0,
	e_link_cmd,
	e_link_crc_1,
	e_link_delay_1,
	e_link_size,
	e_link_data,
	e_link_crc_2 = MAX_DATA_FRAME,
	e_link_delay_2,
	e_link_feedback,
	e_link_idle,
	e_link_finish,
	e_link_error,
};

enum e_link_cmd{
	e_link_cmd_who = 0,
	e_link_cmd_h2n,
	e_link_cmd_n2h,
	e_link_cmd_ping,
};

struct LinkLayer_package{
	unsigned char addr;
	unsigned char cmd;
	unsigned char crc;
	unsigned char size;
	unsigned char buf[MAX_DATA_FRAME];
	unsigned char status;
};

static const char* link_status_text[] = {
	"Address",
	"Command",
	"CRC_1",
	"Dummy for transition 1",
	"Data size",
	"Data",
	"CRC_2",
	"Dummy for transition 2",
  	"Feedback",
	"Host end transaction session"
};

#endif
