#ifndef __NODE_LINK_H_
#define __NODE_LINK_H_ 

#include "linklayer.h"
#include "sim_spi_module.h"

struct LinkNode_t{
  struct simSPI_t PHY_handler;
  struct LinkLayer_package rcv;
  unsigned char tx_size;
  unsigned char *tx_buf;
  unsigned char *p_rec;
  unsigned char soft_ID;
};


int LinkNode_init(struct LinkNode_t *ln);
int LinkNode_deinit(struct LinkNode_t *ln);

int LinkNode_get_package(struct LinkNode_t *ln, void* data, unsigned char data_size);
int LinkNode_send_package(struct LinkNode_t *ln, void* data, unsigned char data_size);

int LinkNode_start_session(struct LinkNode_t *ln);
int LinkNode_stop_session(struct LinkNode_t *ln);

#endif
