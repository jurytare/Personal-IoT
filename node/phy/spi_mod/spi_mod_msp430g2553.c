#include <spi_mod.h>
#include <msp430.h>

void spi_setup_master()
{


  // SPI config
  //Clock Polarity: The inactive state is high
  //MSB First, 8-bit, Master, 3-pin mode, Synchronous
  
    P1DIR |= BIT7;
    P1OUT &= ~BIT7;
    
    UCB0CTL0 |= UCCKPL + UCMSB + UCSYNC + UCMST;
    P1SEL |= BIT5 + BIT6;
    P1SEL2 |= BIT5 + BIT6;
      UCB0CTL1 |= UCSSEL_2;                     // SMCLK
  UCB0BR0 |= 0x02;                          // /2
  UCB0BR1 = 0;    
}

void spi_setup_slave()
{
  UCB0CTL1 = UCSWRST;
  UCB0CTL0 |= UCCKPL + UCMSB + UCSYNC + UCMODE_2;
  UCB0CTL1 &= ~UCSWRST;
  IE2 |= UCB0RXIE;                          // Enable USCI0 RX interrupt
  P1SEL |= BIT5 + BIT7 + BIT6 + BIT4;
  P1SEL2 |= BIT5 + BIT7 + BIT6 + BIT4;
  UCB0TXBUF = 0x00;
}

inline void spi_start()
{
  UCB0CTL1 &= ~UCSWRST; // hold module in reset
}

inline void spi_stop()
{
	UCB0CTL1 = UCSWRST; // hold module in reset
}

void spi_MOSI_to_GPIO()
{
  P1SEL &= ~BIT7;
  P1SEL2 &= ~BIT7;
}

void spi_GPIO_to_MOSI()
{
  P1SEL2 |= BIT7;
  P1SEL |= BIT7;
}

void spi_raise_MOSI()
{
  P1OUT &= ~BIT7;
  P1OUT |= BIT7;
}

inline void spi_send(unsigned char buffer)
{
  UCB0TXBUF = buffer;
}

inline int spi_tx_is_busy()
{
  return (UCB0STAT & UCBUSY);
}

int spi_tx_is_ready()
{
  return (IFG2 & UCB0TXIFG);
}

inline int spi_get()
{
  return UCB0RXBUF;
}

inline int spi_rx_is_valid()
{
  return (IFG2 & UCB0RXIFG);
}

void spi_setup_cs_catcher()
{
  P1DIR &= ~BIT4;
  P1IE |= BIT4;
  P1IES |= BIT4;
  P1REN |= BIT4;
  P1OUT |= BIT4;
  P1IFG &= ~BIT4;
}

void spi_enable_cs_catcher()
{
  P1SEL &= ~BIT4;
  P1SEL2 &= ~BIT4;
  P1IE |= BIT4;
  P1IFG &= ~BIT4;
}

inline void spi_disable_cs_catcher()
{
  P1IE &= ~BIT4;
}