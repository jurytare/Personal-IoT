#include <msp430.h>
#include <spi_mod.h>

int main()
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer

  // Clock config
  if (CALBC1_16MHZ==0xFF)                  // If calibration constant erased
  {
      while(1);                               // do not load, trap CPU!!
  }
  DCOCTL = 0;                               // Select lowest DCOx and MODx settings
  BCSCTL1 = CALBC1_16MHZ;                    // Set DCO
  DCOCTL = CALDCO_16MHZ;

	BCSCTL1 |= XT2OFF;
	BCSCTL2 |= SELM_1 + DIVS_2;
	BCSCTL3 |= LFXT1S_2;

  // SPI config
  spi_stop();
  void spi_setup_master();
  spi_start();



  unsigned char data = 178;
  unsigned int count;
  while(1){
    spi_raise_MOSI();
    spi_GPIO_to_MOSI();

    count = data;
    __delay_cycles(200);
    while(count){
      spi_send(data);
      count--;
      while(!spi_tx_is_ready());
    }
    
    while(spi_tx_is_busy());
    
    spi_MOSI_to_GPIO();
    spi_raise_MOSI();

    __delay_cycles(50);
  }
  return 0;
}

// #include <msp430.h>
// #include <stdint.h>

// int main()
// {
//   WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer

//   // Clock config
//   if (CALBC1_16MHZ==0xFF)                  // If calibration constant erased
//   {
//       while(1);                               // do not load, trap CPU!!
//   }
//   DCOCTL = 0;                               // Select lowest DCOx and MODx settings
//   BCSCTL1 = CALBC1_16MHZ;                    // Set DCO
//   DCOCTL = CALDCO_16MHZ;

// 	BCSCTL1 |= XT2OFF;
// 	BCSCTL2 |= SELM_1 + DIVS_2;
// 	BCSCTL3 |= LFXT1S_2;

//   // SPI config
//   UCB0CTL1 = UCSWRST; // hold module in reset
//   //Clock Polarity: The inactive state is high
//   //MSB First, 8-bit, Master, 3-pin mode, Synchronous
  
//   UCB0CTL0 = UCCKPL + UCMST + UCMSB + UCSYNC;
//   UCB0CTL1 = UCSSEL_2;                     // SMCLK
//   UCB0BR0 = 0x02;                          // /2
//   UCB0BR1 = 0;                              //
//   P1SEL |= BIT5 + BIT7 + BIT6;
//   P1SEL2 |= BIT5 + BIT7 + BIT6;
//   UCB0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**

//   P1DIR |= BIT0;
//   P1OUT |= BIT0;

//   uint8_t data = 178;
//   // P1DIR |= BIT5 + BIT7;

//   while(1){
//     P1OUT &= ~BIT0;
//     // P1OUT &= ~BIT7;
//     // P1OUT |= BIT7;
//     // Wait for other devices preparing for data comming
//     __delay_cycles(100);
    
//     // P1SEL |= BIT7;
//     // P1SEL2 |= BIT7;
//     uint16_t i = 0; 
//     while(i < 0xFFFF){
//       UCB0TXBUF = data;
//       i++;
//       while(UCB0STAT & UCBUSY);
//     }
//     // data = UCB0RXBUF;
//     // P1OUT &= ~BIT7;
//     // P1SEL &= ~BIT7;
//     // P1SEL2 &= ~BIT7;
//     // P1OUT |= BIT7;
//     P1OUT |= BIT0;
//   }
//   return 0;
// }
