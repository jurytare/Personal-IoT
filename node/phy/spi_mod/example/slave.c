#include <stdint.h>
#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <msp430.h>
#include <stdarg.h> 
#include <spi_mod.h>

volatile uint64_t ByteOK, BytesError;
volatile int pre_data;
volatile uint8_t counter, spi_data;
volatile uint8_t is_data;

void hw_init();
void start_timer();

void Send_log(char *, ...);

int main(int argc, char const *argv[])
{
  hw_init();
  start_timer();

  while(1){
      ByteOK = BytesError = 0;
      spi_data = 0;
      __bis_SR_register(CPUOFF + GIE);  
      uint64_t MaxSpeed;
      MaxSpeed = ByteOK + BytesError;
      
      Send_log("Received: ");
      Send_log("%lB", MaxSpeed);
      Send_log("/s, Error: %lB", BytesError);

      if(MaxSpeed > 0){
        double ratioOk = (ByteOK) * 100.0/MaxSpeed;
        Send_log(", OK (%f%%) - %d", ratioOk, (int)spi_data);
        if(ratioOk < 50){
          spi_stop();
          spi_enable_cs_catcher();
        }
      }
      Send_log("\n");
      
  }
  return 0;
}

void hw_init()
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer

  // Clock config
  if (CALBC1_16MHZ==0xFF)                  // If calibration constant erased
  {
      while(1);                               // do not load, trap CPU!!
  }
  DCOCTL = 0;                               // Select lowest DCOx and MODx settings
  BCSCTL1 = CALBC1_16MHZ;                    // Set DCO
  DCOCTL = CALDCO_16MHZ;

  BCSCTL1 |= XT2OFF;
  BCSCTL2 |= SELM_1 + DIVS_2;
  BCSCTL3 |= LFXT1S_2;

  // UART config
  UCA0CTL1 |= UCSWRST;

  UCA0CTL1 |= UCSSEL_2;

  UCA0BR0 = 34;
  UCA0BR1 = 0;
  // UCA0MCTL = UCBRS1 + UCBRS2;

  P1SEL |= BIT1 + BIT2;
  P1SEL2 |= BIT1 + BIT2;

  UCA0CTL1 &= ~UCSWRST;

  IE2 |= UCA0RXIE;

  spi_setup_cs_catcher();
   __bis_SR_register(GIE);
}

void start_timer()
{
  // Timer config
  P1DIR |= 0x01;                            // P1.0 output
  CCTL0 = CCIE;                             // CCR0 interrupt enabled
  CCR0 = 50000;
  TACTL = TASSEL_2 + MC_2 + ID_3;                  // SMCLK, contmode
}

char *inum_to_str(uint64_t value, char *buf, size_t MAX_SIZE)
{
    char *res;
    res = buf + MAX_SIZE;
    *res = '\0';

    do{
      res--;
      *res = (value % 10) + '0';
      value = value / 10;
    }while(value && res >= buf);

    return res;
}

char *fnum_to_str(double value, char *buf, size_t MAX_SIZE, int num_of_dec)
{
    char *res;
    int i, ival;
    double lessThanOne;
    ival = value;
    lessThanOne = value - ival;
    res = inum_to_str(ival, buf, MAX_SIZE);
    if(res == buf || lessThanOne == 0){
        return res;
    }
    memmove(buf, res, MAX_SIZE - (res - buf));
    res = buf + MAX_SIZE - (res - buf);
    *res = '.';
    i = 0;
    do{
      res++;
      lessThanOne = lessThanOne * 10;
      ival = lessThanOne;
      *res = (ival + '0');
      lessThanOne = lessThanOne - ival;
      i++;
    }while(lessThanOne > 0 && i <= num_of_dec && (res - buf <= MAX_SIZE));
    *res = '\0';
    return buf;
}

void Send_log(char *data, ...)
{
  va_list ap;
  va_start(ap, data);
  while(*data){
    if(*data == '%'){
      uint8_t sz_buf;
      char buf[20] = {0};
      char *res;
      data++;
      if(*data == 'l'){
        uint64_t value;
        value = va_arg(ap, uint64_t);
        Send_log(inum_to_str(value, buf, 20));
      }else if(*data == 'd'){
        int value;
        value = va_arg(ap, int);
        Send_log(inum_to_str(value, buf, 20));
      }else if(*data == 'f'){
        double value;
        value = va_arg(ap, double);
        Send_log(fnum_to_str(value, buf, 20, 3));
      }else if(*data == '%'){
        while(!(IFG2 & UCA0TXIFG));
        UCA0TXBUF = *data;
      }
      data++;
    }else{
      while(!(IFG2 & UCA0TXIFG));
      UCA0TXBUF = *data++;
    }
  }
  va_end(ap);
}

// Timer A0 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) Timer_A (void)
#else
#error Compiler not supported!
#endif
{
  CCR0 += 25000;                            // Add Offset to CCR0
  if(++counter > 19){
    P1OUT ^= BIT0;                            // Toggle P1.0
    counter = 0;
    __bic_SR_register_on_exit(CPUOFF + GIE);
  }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCIAB0RX_VECTOR))) USCI0RX_ISR (void)
#else
#error Compiler not supported!
#endif
{
    spi_data = spi_get();

    if(spi_data == 178){
      ByteOK++;
    }else{
      BytesError += (spi_data != 178);
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
#else
#error Compiler not supported!
#endif
{
  spi_disable_cs_catcher();
  spi_setup_slave();
}