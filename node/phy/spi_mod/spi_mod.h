#ifndef __SPI_MOD_H__
#define __SPI_MOD_H__

void spi_setup_master();
void spi_setup_slave();
inline void spi_start();
inline void spi_stop();

void spi_MOSI_to_GPIO();
void spi_GPIO_to_MOSI();
void spi_raise_MOSI();

inline void spi_send(unsigned char buffer);
inline int spi_get();

inline int spi_tx_is_ready();
inline int spi_tx_is_busy();
inline int spi_rx_is_valid();

void spi_setup_cs_catcher();
void spi_enable_cs_catcher();
inline void spi_disable_cs_catcher();

#endif