#ifndef __SIM_SPI_MODULE__
#define __SIM_SPI_MODULE__

#define SIM_SPI_POLLING

#include <pthread.h>
#include <semaphore.h>

#define BIT(x) ((0x01 << x))

enum simSPI_status_e{
  simSPI_status_isGoing = 0,
  simSPI_status_isPaused,
  simSPI_status_RxHasData,
  simSPI_status_TxIsBusy,
  simSPI_status_hasNotif
};

enum simSPI_mode_e{
  simSPI_mode_host = 0,
  simSPI_mode_TX,
  simSPI_mode_TRX_isr,
  simSPI_mode_start_isr
};

struct simSPI_t{
	unsigned char mode;
	int tx_buf;
	unsigned char rx_buf;
	enum simSPI_status_e status;

	pthread_t thread;
  sem_t locker;
  // pthread_mutex_t thread_mutex_locker;
  // pthread_cond_t thread_cond_locker;
  void *param;
  
  int (*sim_spi_isr)(struct simSPI_t *ss);
};

/* 
 * This is used for create a simSPI thread which is communicate 
 * with other simSPI threads.
 * Params:
 * struct simSPI_t ss: simSPI object for initialization
 * uint8_t mode: host or node
 * Return:
 *   == 0 for sucess
 *   < 0 for errors
 *   > 0 for warnings
 */
int simSPI_init(struct simSPI_t *ss, unsigned char mode);
int simSPI_deinit(struct simSPI_t *ss);

int simSPI_setMode(struct simSPI_t *ss, enum simSPI_mode_e mode);
int simSPI_clearMode(struct simSPI_t *ss, enum simSPI_mode_e mode);

int simSPI_data_send(struct simSPI_t *ss, unsigned char data);
int simSPI_data_get(struct simSPI_t *ss, unsigned char *data);

int simSPI_register_isr(struct simSPI_t *ss, enum simSPI_mode_e isr_val);
int simSPI_unregister_isr(struct simSPI_t *ss, enum simSPI_mode_e isr_val);
int simSPI_set_isr_callback(struct simSPI_t *ss,
    int (*callback)(struct simSPI_t *ss));

int simSPI_start(struct simSPI_t *ss);
int simSPI_stop(struct simSPI_t *ss);

#endif
