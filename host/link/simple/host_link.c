#include "host_link.h"
#include <stdint.h>
#include <string.h>
#include "slog.h"

//static const char* HARDWARE_ID = "0001";

static int link_switch_rx2tx(struct LinkHost_t *lh)
{
	SLOG_D("");
	simSPI_setMode(&lh->PHY_handler, simSPI_mode_TX);
	return 0;
}

static int link_switch_tx2rx(struct LinkHost_t *lh)
{
	SLOG_D("");
	simSPI_clearMode(&lh->PHY_handler, simSPI_mode_TX);
	return 0;
}

static int link_on_event_send(struct LinkHost_t *lh, unsigned char *sendIt)
{
	int ret;
	ret = 0;

	if(lh->transfer.status < e_link_data){
		SLOG_I("%d %s", lh->transfer.status, link_status_text[lh->transfer.status]);
	}else if( e_link_crc_2 > lh->transfer.status){
		SLOG_I("%d %s %c", lh->transfer.status, link_status_text[e_link_data], *lh->p_rec);
	}else{
    SLOG_I("%d %s", lh->transfer.status, link_status_text[lh->transfer.status - e_link_crc_2 + e_link_data + 1]);
  }
  
  switch(lh->transfer.status){
    case e_link_crc_1:
      *sendIt = lh->transfer.crc ^ CRC_IDENTITY;
      SLOG_I("Send crc = %d", *sendIt);
      lh->transfer.crc = 0;
		switch(lh->transfer.cmd){
			case e_link_cmd_h2n:
				lh->transfer.status = e_link_size;
				lh->p_rec = &lh->transfer.size;
			break;
			case e_link_cmd_n2h:
				lh->transfer.status = e_link_delay_1;
				lh->p_rec = &lh->transfer.size;
			break;
			case e_link_cmd_ping:
				lh->transfer.status = e_link_delay_2;
				lh->p_rec = &lh->transfer.crc;
			break;
			default:
				SLOG_E("Unknown command");
			break;
		}
    break;
    case e_link_delay_1:
    	lh->transfer.status = e_link_size;
    	link_switch_tx2rx(lh);
      ret = 1;
    break;
    case e_link_delay_2:
    	lh->transfer.status = e_link_feedback;
    	link_switch_tx2rx(lh);
      ret = 1;
    break;
    case e_link_feedback:
    	*sendIt = lh->transfer.crc ^ CRC_IDENTITY;
    	lh->transfer.status = e_link_finish;
    	// Todo: link_stop is not stopping imediately
    	LinkHost_stop_session(lh);
    break;
    case e_link_error:
    	*sendIt = lh->transfer.crc ^ CRC_IDENTITY;
    	LinkHost_stop_session(lh);
    break;
    case e_link_finish:
    	LinkHost_stop_session(lh);
    	SLOG_E("No need to transfer anymore");
    break;
    default:
      if(lh->transfer.status - e_link_data == lh->transfer.size){
        SLOG_D("Complete sending. Send CRC_2 now");
        *sendIt = lh->transfer.crc;
        lh->transfer.status = e_link_delay_2;
      }else if(lh->transfer.status > e_link_finish){
        LinkHost_stop_session(lh);
        ret = -2;
      }else{
        SLOG_D("Send %d", *lh->p_rec); 
        *sendIt = *lh->p_rec++;
        lh->transfer.status++;
        lh->transfer.crc ^= *sendIt;
      }
  }
	return ret;
}

static int link_on_event_rcv(struct LinkHost_t *lh, unsigned char data)
{
  if(lh->transfer.status < e_link_data){
    SLOG_I("%d %s", lh->transfer.status, link_status_text[lh->transfer.status]);
  }else if( e_link_crc_2 > lh->transfer.status){
    SLOG_I("%d %s %c", lh->transfer.status, link_status_text[e_link_data], data);
  }else{
    SLOG_I("%d %s", lh->transfer.status, link_status_text[lh->transfer.status - e_link_crc_2 + e_link_data + 1]);
  }

  switch(lh->transfer.status){

    case e_link_feedback:
    lh->transfer.status = e_link_finish + ((lh->transfer.crc ^ data) != CRC_IDENTITY);
    SLOG_I("End of transaction with result %d (CRC:%d)", lh->transfer.status, lh->transfer.crc);
    LinkHost_stop_session(lh);
    break;
    default:
      if(lh->transfer.status - e_link_data == lh->transfer.size){
        SLOG_D("Complete recording");
        lh->transfer.status = e_link_feedback;
        if(lh->transfer.crc != data){
        	// Todo: something here to inform that data has been corrupted
        }
        link_switch_rx2tx(lh);
      }else if(lh->transfer.status < sizeof(struct LinkLayer_package)){
        SLOG_D("Record: Data=0x%.2x, status=%d, crc=0x%.2x", data, lh->transfer.status, lh->transfer.crc);
        *lh->p_rec++ = data;
        lh->transfer.status++;
        lh->transfer.crc ^= data;
      }else{
        lh->transfer.status = e_link_error;
        LinkHost_stop_session(lh);
      }
    break;
  }
	return 0;
}

static int link_layer_events(struct simSPI_t *ss)
{
	SLOG_D("");
	if ((ss->mode && BIT(simSPI_mode_start_isr)) \
			&& (ss->status & BIT(simSPI_status_hasNotif))){
		LinkHost_start_session((struct LinkHost_t*)ss);
	}

	unsigned char ch;
	if (ss->mode & BIT(simSPI_mode_TX)){
		if(!(ss->status & BIT(simSPI_status_TxIsBusy))){
			if(!link_on_event_send((struct LinkHost_t*)ss, &ch)){
				simSPI_data_send(ss, ch);
			}
		}
	}else{
		if(ss->status & BIT(simSPI_status_RxHasData)){
			simSPI_data_get(ss, &ch);
			link_on_event_rcv((struct LinkHost_t*)ss, ch);
		}
	}
}

int LinkHost_init(struct LinkHost_t *lh)
{
	int ret;
	lh->transfer.status = lh->transfer.size = 0;
	/* For specific PHY layer*/
	ret = simSPI_init(&lh->PHY_handler, BIT(simSPI_mode_host));
	if(ret){
		return ret;
	}
	simSPI_register_isr(&lh->PHY_handler, simSPI_mode_TRX_isr);
	simSPI_set_isr_callback(&lh->PHY_handler, &link_layer_events);
	return 0;
}

int LinkHost_deinit(struct LinkHost_t *lh)
{
	SLOG_D("");
	simSPI_deinit(&lh->PHY_handler);
	return 0;
}

int LinkHost_start_session(struct LinkHost_t *lh)
{
	SLOG_D("");
	uint8_t temp;
	lh->p_rec = (uint8_t *)&lh->transfer;
	lh->transfer.status = lh->transfer.crc = 0;
	simSPI_register_isr(&lh->PHY_handler, simSPI_mode_TRX_isr);
	simSPI_data_get(&lh->PHY_handler, &temp);
	link_switch_rx2tx(lh);
	simSPI_start(&lh->PHY_handler);
}

int LinkHost_stop_session(struct LinkHost_t *lh)
{
	SLOG_D("Host stop transaction");
  	simSPI_unregister_isr(&lh->PHY_handler, simSPI_mode_TRX_isr);
    simSPI_stop(&lh->PHY_handler);
}

int LinkHost_send_package(struct LinkHost_t *lh, unsigned char dst_adr, void* data, unsigned char data_size)
{
	lh->transfer.cmd = e_link_cmd_h2n;
	lh->transfer.addr = dst_adr;
	lh->transfer.crc = 0;
	lh->transfer.size = data_size;
	memcpy(lh->transfer.buf, data, data_size);
//	lh->transfer.buf = data;
  SLOG_I("Start sending %s, length =  %d", data, data_size);
 
  LinkHost_start_session(lh);
  while(lh->transfer.status < e_link_finish);
	return (e_link_finish == lh->transfer.status);
}

int LinkHost_get_package(struct LinkHost_t *lh, unsigned char src_adr, void* data, unsigned char data_size)
{
  lh->transfer.cmd = e_link_cmd_n2h;
  lh->transfer.addr = src_adr;
  lh->transfer.crc = 0;
  lh->transfer.size = MAX_DATA_FRAME;
  LinkHost_start_session(lh);
  while(e_link_finish != lh->transfer.status);
  unsigned char data_cpy = data_size < lh->transfer.size? \
      data_size : lh->transfer.size;
  memcpy(data, lh->transfer.buf, data_cpy);
  lh->transfer.status = e_link_idle;
  return data_cpy;
}
