#ifndef __HOST_LINK_H_
#define __HOST_LINK_H_ 

#include "linklayer.h"
#include "sim_spi_module.h"

struct LinkHost_t{
	struct simSPI_t PHY_handler;
	struct LinkLayer_package transfer;
	unsigned char *p_rec;
};


int LinkHost_init(struct LinkHost_t *lh);
int LinkHost_deinit(struct LinkHost_t *lh);

int LinkHost_get_package(struct LinkHost_t *lh, unsigned char src_adr, void* data, unsigned char data_size);
int LinkHost_send_package(struct LinkHost_t *lh, unsigned char dst_adr, void* data, unsigned char data_size);

int LinkHost_start_session(struct LinkHost_t *lh);
int LinkHost_stop_session(struct LinkHost_t *lh);

#endif
