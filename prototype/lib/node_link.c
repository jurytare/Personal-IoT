#include "node_link.h"
#include <stdint.h>
#include <string.h>
#include "slog.h"
#include <unistd.h>

//static const char* HARDWARE_ID = "0001";

static int link_switch_rx2tx(struct LinkNode_t *ln)
{
	SLOG_D("");
	simSPI_setMode(&ln->PHY_handler, simSPI_mode_TX);
	return 0;
}

static int link_switch_tx2rx(struct LinkNode_t *ln)
{
	SLOG_D("");
	simSPI_clearMode(&ln->PHY_handler, simSPI_mode_TX);
	return 0;
}

static int link_on_event_rcv(struct LinkNode_t *ln, unsigned char data)
{
  if(ln->rcv.status < e_link_data){
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status]);
  }else if( ln->rcv.status - e_link_data < ln->rcv.size){
    SLOG_I("%d %s %c", ln->rcv.status, link_status_text[e_link_data], data);
  }else if( ln->rcv.status < e_link_crc_2) {
    SLOG_I("%d %s", ln->rcv.status, link_status_text[e_link_data + 1]);
  }else{
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status - e_link_crc_2 + e_link_data + 1]);
  }

  switch(ln->rcv.status){
    case e_link_address:
      ln->rcv.addr = data;
      ln->rcv.status = e_link_cmd;
      ln->rcv.crc = ln->rcv.addr;
    break;
    case e_link_cmd:
      ln->rcv.cmd = data;
      ln->rcv.status = e_link_crc_1;
      ln->rcv.crc ^= ln->rcv.cmd;
    break;
    case e_link_crc_1:
      if((ln->rcv.crc ^ data) == CRC_IDENTITY){
        ln->rcv.crc = 0;
        /* Check address of the package if this address is for this node or not */
        if(ln->rcv.addr == ln->soft_ID){
          SLOG_D("Processing header %d", ln->rcv.cmd);
          switch(ln->rcv.cmd){
            case e_link_cmd_h2n:
				      ln->rcv.status = e_link_size;
            break;
            case e_link_cmd_n2h:
            	ln->rcv.status = e_link_size;
            	link_switch_rx2tx(ln);
            break;
            case e_link_cmd_ping:
            	ln->rcv.status = e_link_feedback;
            	link_switch_rx2tx(ln);
            break;
            default:
              SLOG_W("Unknown command");
            break;
          }
          return 0;
        }else{
          SLOG_W("Different address: %d", ln->rcv.addr);
          LinkNode_stop_session(ln);
          ln->rcv.status = e_link_idle;
        }
      }else{
        /* Failed to receive header */
        SLOG_E("Failed to detect CRC_0: %d & %d", data, ln->rcv.crc);
        LinkNode_stop_session(ln);
        ln->rcv.status = e_link_idle;
        /* Should have a counter for failures.
         * If it fails too much, consider to reset.
         */
      }
    break;
    case e_link_size:
      if(data > MAX_DATA_FRAME){
        LinkNode_stop_session(ln);
        ln->rcv.status = e_link_error;
        break;
      }
      ln->rcv.size = data;
      ln->rcv.status = e_link_data;
      ln->p_rec = ln->rcv.buf;
      ln->rcv.crc = ln->rcv.size;
    break;
    case e_link_feedback:
      LinkNode_stop_session(ln);
      data = ln->rcv.crc ^ data;
      if(data != CRC_IDENTITY){
        ln->rcv.status = e_link_error;
      }else{
        ln->tx_size = 0;
        ln->rcv.status = e_link_finish;
      }
	    SLOG_I("End of transaction with result %d (CRC:%d)", ln->rcv.status, ln->rcv.crc);
    break;
    default:
      if(ln->rcv.status - e_link_data == ln->rcv.size){
        SLOG_D("Complete recording. Check CRC_2 now");
        if(ln->rcv.crc != data){
          SLOG_D("Something wrong here! CRC=%d and data=%d", ln->rcv.crc, data);
        }
        ln->rcv.status = e_link_feedback;
        link_switch_rx2tx(ln);
      }else if(ln->p_rec < &ln->rcv.status){
        SLOG_D("Record: Data=0x%.2x, status=%d, crc=0x%.2x", data, ln->rcv.status, ln->rcv.crc);
        *ln->p_rec++ = data;
        ln->rcv.status++;
        ln->rcv.crc ^= data;
      }else{
        LinkNode_stop_session(ln);
        ln->rcv.status = e_link_error;

      }
    break;
  }
	return 0;
}

static int link_on_event_send(struct LinkNode_t *ln, unsigned char *sendIt)
{
  int ret;
  ret = 0;

  if(ln->rcv.status < e_link_data){
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status]);
  }else if( ln->rcv.status - e_link_data < ln->tx_size){
    SLOG_I("%d %s %c", ln->rcv.status, link_status_text[e_link_data], *ln->p_rec);
  }else if( ln->rcv.status < e_link_crc_2) {
    SLOG_I("%d %s", ln->rcv.status, link_status_text[e_link_data + 1]);
  }else{
    SLOG_I("%d %s", ln->rcv.status, link_status_text[ln->rcv.status - e_link_crc_2 + e_link_data + 1]);
  }

  switch(ln->rcv.status){
    case e_link_size:
      *sendIt = ln->tx_size;
      ln->rcv.status = e_link_data;
      ln->p_rec = ln->tx_buf;
      ln->rcv.crc = ln->tx_size;
    break;
    case e_link_delay_2:
      ln->rcv.status = e_link_feedback;
      link_switch_tx2rx(ln);
      ret = 1;
      break;
    case e_link_feedback:
      *sendIt = ln->rcv.crc ^ CRC_IDENTITY;
      SLOG_D("Feedback CRC = %d", *sendIt);
      LinkNode_stop_session(ln);
      ln->rcv.status = e_link_finish;
    break;
    default:
      if(ln->rcv.status - e_link_data == ln->tx_size){
        ln->rcv.status = e_link_delay_2;
        *sendIt = ln->rcv.crc;
        SLOG_D("Complete sending. Send CRC = %d", ln->rcv.crc);
      }else if(ln->rcv.status >= e_link_finish){
        LinkNode_stop_session(ln);
        ret = -2;
      }else{
        SLOG_D("Send %d", *ln->p_rec);
        *sendIt = *ln->p_rec++;
        ln->rcv.status++;
        ln->rcv.crc ^= *sendIt;
      }
  }
  return ret;
}

static int link_layer_events(struct simSPI_t *ss)
{
	SLOG_D("");
	if ((ss->mode && BIT(simSPI_mode_start_isr)) \
			&& (ss->status & BIT(simSPI_status_hasNotif))){
		LinkNode_start_session((struct LinkNode_t*)ss);
	}

	unsigned char ch;
	if (ss->mode & BIT(simSPI_mode_TX)){
		if(!(ss->status & BIT(simSPI_status_TxIsBusy))){
			if(!link_on_event_send((struct LinkNode_t*)ss, &ch)){
				simSPI_data_send(ss, ch);
			}
		}
	}else{
		if(ss->status & BIT(simSPI_status_RxHasData)){
			simSPI_data_get(ss, &ch);
			link_on_event_rcv((struct LinkNode_t*)ss, ch);
		}
	}
}

int LinkNode_init(struct LinkNode_t *ln)
{
	int ret;
	ln->rcv.size = ln->tx_size = 0;
	ln->tx_buf = NULL;
	ln->soft_ID = 32;
	/* For specific PHY layer*/
	ret = simSPI_init(&ln->PHY_handler, 0);
	if(ret){
		return ret;
	}
	simSPI_register_isr(&ln->PHY_handler, simSPI_mode_start_isr);
	simSPI_set_isr_callback(&ln->PHY_handler, &link_layer_events);
	LinkNode_start_session(ln);
	return 0;
}

int LinkNode_deinit(struct LinkNode_t *ln)
{
	SLOG_D("");
	simSPI_deinit(&ln->PHY_handler);
	return 0;
}

int LinkNode_start_session(struct LinkNode_t *ln)
{
	SLOG_D("");
	uint8_t temp;
	ln->rcv.status = ln->rcv.crc = 0;
	ln->rcv.size = MAX_DATA_FRAME;
	simSPI_register_isr(&ln->PHY_handler, simSPI_mode_TRX_isr);
	simSPI_data_get(&ln->PHY_handler, &temp);
	link_switch_tx2rx(ln);
	simSPI_start(&ln->PHY_handler);
}

int LinkNode_stop_session(struct LinkNode_t *ln)
{
	SLOG_D("Node stop transaction");
  	simSPI_unregister_isr(&ln->PHY_handler, simSPI_mode_TRX_isr);
}

int LinkNode_send_package(struct LinkNode_t *ln, void* data, unsigned char data_size)
{
	ln->tx_size = data_size;
	ln->tx_buf = data;
	SLOG_I("Start sending %s, length =  %d", data, data_size);

	while(ln->rcv.status != e_link_finish || ln->rcv.cmd != e_link_cmd_n2h){
    usleep(10000);
  }
	return 0;
}

int LinkNode_get_package(struct LinkNode_t *ln, void* data, unsigned char data_size)
{
  while(ln->rcv.status != e_link_finish || ln->rcv.cmd != e_link_cmd_h2n){
    usleep(10000);
  }
  unsigned char data_cpy = data_size < ln->rcv.size? \
      data_size : ln->rcv.size;
  memcpy(data, ln->rcv.buf, data_cpy);
  ln->rcv.status = e_link_address;
  return data_cpy;
}