#include "sim_spi_module.h"

#ifdef SIM_SPI_SEM

#define  _BSD_SOURCE
#include <pthread.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
// #include <stdio.h>
#define FILE_KEY "file_key"
#define SEM_FLAGS   O_CREAT
#define SEM_PERMS   S_IRUSR | S_IWUSR
#include <stdint.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
struct _simSPI_data_t{
  uint8_t  startData;
  unsigned char DATA;
  int     new_node_id;
};
struct sim_spi_node_t{
  int pid;
  sem_t *sem;
  struct sim_spi_node_t *next;
};
static struct _simSPI_data_t *_local_sim_spi_;

sem_t *sim_spi_wait_notif_sem;
struct sim_spi_node_t *head;



static void sim_spi_new_node(int signo)
{
  if(signo == SIGUSR1)
  {
    struct sim_spi_node_t *node = (struct sim_spi_node_t*) \
          malloc(sizeof(struct sim_spi_node_t));
        node->pid = _local_sim_spi_->new_node_id;
        _local_sim_spi_->new_node_id = 0;
        printf("There is new node at %d\n", node->pid); 
        
        char semname[10];
        sprintf(semname, "%d", node->pid);
        node->sem = sem_open(semname, SEM_FLAGS, SEM_PERMS, 0);
        node->next = head;
        head = node;

  }
}
static void sim_spi_send_notif(struct simSPI_t *ss)
{

  struct sim_spi_node_t *it;
  it = head;
  _local_sim_spi_->startData = (ss->status & BIT(simSPI_status_hasNotif));
  if(ss->status & BIT(simSPI_status_hasNotif))
    ss->status &= ~BIT(simSPI_status_hasNotif);
  while(it)
  {
    if(sem_post(it->sem) == -1)
      printf("A node died\n");
    it = it->next;
  }
  // printf("clk %d, data %d\n", _local_sim_spi_->CLK, _local_sim_spi_->DATA );
  usleep(10000);
}
static int sim_spi_wait_notif(struct simSPI_t *ss)
{
  if(_local_sim_spi_->startData & BIT(simSPI_status_hasNotif)){
    ss->status |= BIT(simSPI_status_hasNotif);
    if((ss->mode & BIT(simSPI_mode_start_isr)) && \
        ss->sim_spi_isr){
      // printf("notif_isr\n");
      ss->sim_spi_isr(ss);
      ss->status &= ~BIT(simSPI_status_hasNotif);
    } 
  }
  sem_wait(sim_spi_wait_notif_sem);
  return 0;
}


static void sim_spi_write(struct simSPI_t *ss)
{
  _local_sim_spi_->DATA = ss->tx_buf;
  ss->status &= ~BIT(simSPI_status_TxIsBusy);
}

static void sim_spi_read(struct simSPI_t *ss)
{
  ss->rx_buf = _local_sim_spi_->DATA;
  ss->status |= BIT(simSPI_status_RxHasData);
}

void* sim_spi_main_thread(void* arg)
{
  struct simSPI_t *sim_spi = arg;
  // unsigned char isTxOnBus = 0;
  while(sim_spi->status & BIT(simSPI_status_isGoing)){
     //sim_spi->status |= BIT(simSPI_status_isPaused);
    while(sim_spi->status & BIT(simSPI_status_isPaused)){
      sem_wait(&sim_spi->locker);
    }

    if(sim_spi->mode & BIT(simSPI_mode_TX)){
      if(sim_spi->sim_spi_isr && 
        (sim_spi->mode & BIT(simSPI_mode_TRX_isr))){
        if(!(sim_spi->status & BIT(simSPI_status_TxIsBusy))){
          sim_spi->sim_spi_isr(sim_spi);
        }
      }
      if(sim_spi->status & BIT(simSPI_status_TxIsBusy)){
        // printf("tx_isr\n");
        sim_spi_write(sim_spi);
        // isTxOnBus = 1;
      }
    }
    
    if(sim_spi->mode & BIT(simSPI_mode_host)){
      // if(isTxOnBus){
        //printf("h\n");
        sim_spi_send_notif(sim_spi);
        // isTxOnBus = 0;
      // }
    }else{
      sim_spi_wait_notif(sim_spi);
      //sleep(2);
        //break;
    }
    
    if(!(sim_spi->mode & BIT(simSPI_mode_TX))){
      sim_spi_read(sim_spi);
      if(sim_spi->sim_spi_isr &&
        (sim_spi->mode & BIT(simSPI_mode_TRX_isr))){
        // printf("rx_isr\n");
        sim_spi->sim_spi_isr(sim_spi);
      }
    }
  }

  pthread_exit(NULL);
}

int simSPI_init(struct simSPI_t *ss, unsigned char mode)
{
  /* This part could be change if share memory is not suitable*/
  pid_t keypid;
  int _shmid;
  FILE *f_in;
  if((int)mode == 1)
  {
    keypid = getpid();
    f_in = fopen(FILE_KEY, "w");
    if(!f_in)
    {
        printf("error\n");
        return -1;
    }
    fprintf(f_in, "%d\n", keypid);
  }
  else{
    f_in = fopen(FILE_KEY, "r");
    if(!f_in)
      return -1;
    fscanf(f_in, "%d", &keypid);
  }
  fclose(f_in);
  if (keypid == -1){
    // printf("cannot create key \n");
    return -1;
  }
  _shmid = shmget(keypid, sizeof(struct _simSPI_data_t),
	IPC_CREAT | S_IRUSR | S_IWUSR);

  if (_shmid == -1)  return (-1);

  _local_sim_spi_ = shmat(_shmid, NULL, 0);
 // _local_sim_spi_cur_clk = _local_sim_spi_->CLK;
  
  
  sim_spi_wait_notif_sem = NULL;
  if(signal(SIGUSR1, sim_spi_new_node) == SIG_ERR)
      return -3;
  if((int)mode == 1)
  {
    _local_sim_spi_->DATA = 0;
    _local_sim_spi_->new_node_id = 0;
    head = NULL;
  }
  else{
    int i;
    i = 100;
    while(_local_sim_spi_->new_node_id && i){
      usleep(1000);
      i--;
    }
    if(!i)
    {
      printf("Failed to wait host\n");
      return -3;
    }
  _local_sim_spi_->new_node_id = getpid();
  char semname[10];
  sprintf(semname, "%d", (int)getpid());
  sim_spi_wait_notif_sem = sem_open(semname, SEM_FLAGS, SEM_PERMS,0);
  //printf("no err\n");
  kill(keypid, SIGUSR1);

  }
  /* Initialize thread */
  ss->mode = mode;
  ss->status = BIT(simSPI_status_isGoing);
  ss->sim_spi_isr = NULL;
  sem_init(&ss->locker, 0, 0);
  ss->status |= BIT(simSPI_status_isPaused);
  if(pthread_create(&ss->thread,NULL,&sim_spi_main_thread,ss))
  {
	  // printf("cannot create thread \n");
	  return -2;
  }

  return 0;
}

int simSPI_deinit(struct simSPI_t *ss)
{
  void *pthread_ret;

  ss->status = 0;
    pthread_cancel(ss->thread);
  sem_destroy(&ss->locker);
  
  if(_local_sim_spi_){
    shmdt(_local_sim_spi_);
  }
  return 0;
}

int simSPI_register_isr(struct simSPI_t *ss, enum simSPI_mode_e isr_val)
{
  ss->mode |= BIT(isr_val);
  return 0;
}

int simSPI_unregister_isr(struct simSPI_t *ss, enum simSPI_mode_e isr_val)
{
  ss->mode &= ~BIT(isr_val);
  return 0;
}

int simSPI_set_isr_callback(struct simSPI_t *ss,
    int (*callback)(struct simSPI_t *ss)){
  ss->sim_spi_isr = callback;
  return 0;
}
    
int simSPI_start(struct simSPI_t *ss)
{
  if(ss->status & BIT(simSPI_status_isPaused)){
    ss->status &= ~BIT(simSPI_status_isPaused);
    sem_post(&ss->locker);
  }

  if(ss->mode & BIT(simSPI_mode_host)){
    ss->status |= BIT(simSPI_status_hasNotif);
  }
}

int simSPI_stop(struct simSPI_t *ss)
{
  if(!(ss->status & BIT(simSPI_status_isPaused))){
    ss->status |= BIT(simSPI_status_isPaused);
    sem_post(&ss->locker);
  }
  return 0;
}

int simSPI_setMode(struct simSPI_t *ss, enum simSPI_mode_e mode)
{
  ss->mode |= BIT(mode);
  return 0;
}

int simSPI_clearMode(struct simSPI_t *ss, enum simSPI_mode_e mode)
{
  ss->mode &= ~BIT(mode);
  return 0;
}

int simSPI_data_send(struct simSPI_t *ss, unsigned char data)
{
  ss->tx_buf = data;
  ss->status |= BIT(simSPI_status_TxIsBusy);
  return 0;
}

int simSPI_data_get(struct simSPI_t *ss, unsigned char *data)
{
  *data = ss->rx_buf;
  ss->status &= ~BIT(simSPI_status_RxHasData);
  return 0;
}

#endif