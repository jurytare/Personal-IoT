#include "sim_spi_module.h"

#ifdef SIM_SPI_POLLING

#include <pthread.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include "slog.h"

#define FILE_KEY "file_key"


struct _simSPI_data_t{
  unsigned char CLK;
  unsigned char DATA;
};
static struct _simSPI_data_t *_local_sim_spi_;
static unsigned char _local_sim_spi_cur_clk;

static void sim_spi_send_notif(struct simSPI_t *ss)
{
  unsigned char clk_temp;
  clk_temp = (_local_sim_spi_->CLK ^ 0x01) & 0x01;
  _local_sim_spi_->CLK = clk_temp | (ss->status & BIT(simSPI_status_hasNotif));
  ss->status &= ~BIT(simSPI_status_hasNotif);
  SLOG_D("clk %d, data %d", _local_sim_spi_->CLK, _local_sim_spi_->DATA );
  usleep(10000);
}

static int sim_spi_wait_notif(struct simSPI_t *ss)
{
  while(_local_sim_spi_->CLK == _local_sim_spi_cur_clk){
    usleep(1000);
    if(!(ss->status & BIT(simSPI_status_isGoing))){
      return -1;
    }
  }
  _local_sim_spi_cur_clk = _local_sim_spi_->CLK;
  SLOG_D("clk %d, data %d", _local_sim_spi_->CLK, _local_sim_spi_->DATA );
  if(_local_sim_spi_cur_clk & BIT(simSPI_status_hasNotif)){
    ss->status |= BIT(simSPI_status_hasNotif);
    if((ss->mode & BIT(simSPI_mode_start_isr)) && \
        ss->sim_spi_isr){
      SLOG_D("notif_isr");
      ss->sim_spi_isr(ss);
      ss->status &= ~BIT(simSPI_status_hasNotif);
    } 
  }
  return 0;
}


static void sim_spi_write(struct simSPI_t *ss)
{
  _local_sim_spi_->DATA = ss->tx_buf;
  ss->status &= ~BIT(simSPI_status_TxIsBusy);
}

static void sim_spi_read(struct simSPI_t *ss)
{
  ss->rx_buf = _local_sim_spi_->DATA;
  ss->status |= BIT(simSPI_status_RxHasData);
}

void* sim_spi_main_thread(void* arg)
{
  struct simSPI_t *sim_spi = arg;

  while(sim_spi->status & BIT(simSPI_status_isGoing)){
    while(sim_spi->status & BIT(simSPI_status_isPaused)){
      sem_wait(&sim_spi->locker);
    }

    if(sim_spi->mode & BIT(simSPI_mode_TX)){
      if(sim_spi->sim_spi_isr && 
        (sim_spi->mode & BIT(simSPI_mode_TRX_isr))){
        if(!(sim_spi->status & BIT(simSPI_status_TxIsBusy))){
          sim_spi->sim_spi_isr(sim_spi);
        }
      }
      if(sim_spi->status & BIT(simSPI_status_TxIsBusy)){
        SLOG_D("tx_isr");
        sim_spi_write(sim_spi);
      }
    }
    
    if(sim_spi->mode & BIT(simSPI_mode_host)){
        sim_spi_send_notif(sim_spi);
    }else{
      if(sim_spi_wait_notif(sim_spi)){
        break;
      }
    }
    
    if(!(sim_spi->mode & BIT(simSPI_mode_TX))){
      sim_spi_read(sim_spi);
      if(sim_spi->sim_spi_isr &&
        (sim_spi->mode & BIT(simSPI_mode_TRX_isr))){
        SLOG_D("rx_isr");
        sim_spi->sim_spi_isr(sim_spi);
      }
    }
  }

  pthread_exit(NULL);
}

int simSPI_init(struct simSPI_t *ss, unsigned char mode)
{
  /* This part could be change if share memory is not suitable*/
  key_t key;
  int _shmid;
  key = ftok(FILE_KEY, 'x');
  if (key == -1){
    SLOG_E("cannot create key");
    return -1;
  }
  _shmid = shmget(key, sizeof(struct _simSPI_data_t),
	IPC_CREAT | S_IRUSR | S_IWUSR);
  if (_shmid == -1)  return (-1);

  _local_sim_spi_ = shmat(_shmid, NULL, 0);
  _local_sim_spi_cur_clk = _local_sim_spi_->CLK;
  
  /* Initialize thread */
  ss->mode = mode;
  ss->status = BIT(simSPI_status_isGoing);
  ss->sim_spi_isr = NULL;
  
  sem_init(&ss->locker, 0, 0);
  ss->status |= BIT(simSPI_status_isPaused);
  if(pthread_create(&ss->thread,NULL,&sim_spi_main_thread,ss))
  {
	  SLOG_E("cannot create thread");
	  return -2;
  }
  SLOG_I("Finish");
  return 0;
}

int simSPI_deinit(struct simSPI_t *ss)
{
  void *pthread_ret;

  ss->status = 0;
  // if(ss->status & BIT(simSPI_status_isPaused)){
    pthread_cancel(ss->thread);
  // }else{
    // printf("Wait for running thread\n");
    // pthread_join(ss->thread, &pthread_ret);
  // }
  sem_destroy(&ss->locker);
  
  if(_local_sim_spi_){
    shmdt(_local_sim_spi_);
  }
  return 0;
}

int simSPI_register_isr(struct simSPI_t *ss, enum simSPI_mode_e isr_val)
{
  ss->mode |= BIT(isr_val);
  return 0;
}

int simSPI_unregister_isr(struct simSPI_t *ss, enum simSPI_mode_e isr_val)
{
  ss->mode &= ~BIT(isr_val);
  return 0;
}

int simSPI_set_isr_callback(struct simSPI_t *ss,
    int (*callback)(struct simSPI_t *ss)){
  ss->sim_spi_isr = callback;
  return 0;
}
    
int simSPI_start(struct simSPI_t *ss)
{
  if(ss->status & BIT(simSPI_status_isPaused)){
    ss->status &= ~BIT(simSPI_status_isPaused);
    sem_post(&ss->locker);
  }

  if(ss->mode & BIT(simSPI_mode_host)){
    ss->status |= BIT(simSPI_status_hasNotif);
  }
}

int simSPI_stop(struct simSPI_t *ss)
{
  if(!(ss->status & BIT(simSPI_status_isPaused))){
    ss->status |= BIT(simSPI_status_isPaused);
    sem_post(&ss->locker);
  }
  return 0;
}

int simSPI_setMode(struct simSPI_t *ss, enum simSPI_mode_e mode)
{
  ss->mode |= BIT(mode);
  return 0;
}

int simSPI_clearMode(struct simSPI_t *ss, enum simSPI_mode_e mode)
{
  ss->mode &= ~BIT(mode);
  return 0;
}

int simSPI_data_send(struct simSPI_t *ss, unsigned char data)
{
  ss->tx_buf = data;
  ss->status |= BIT(simSPI_status_TxIsBusy);
  return 0;
}

int simSPI_data_get(struct simSPI_t *ss, unsigned char *data)
{
  *data = ss->rx_buf;
  ss->status &= ~BIT(simSPI_status_RxHasData);
  return 0;
}

#endif