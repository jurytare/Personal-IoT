#include <stdlib.h>
#include <unistd.h>
#include <stdio.h> 
#include <stdint.h>
#include <sys/time.h>

#include <node_link.h>
#include <slog.h>

unsigned int ByteOK, ByteError;
unsigned char isRun;
unsigned char test_data;

int main(int argc, char **argv)
{
    ByteOK = ByteError = 0;
    isRun = 1;
    if(argc > 1){
      if(slog_init(argv[1], 0, slog_all_flag)){
        return -1;
      }
    }else{
      if(slog_init("node.log", 0, slog_all_flag)){
        return -1;
      }
    }
    struct timeval start, stop;
    double msecs;
    struct LinkNode_t link;
    LinkNode_init(&link);
    LinkNode_start_session(&link);
    char read_buf[MAX_DATA_FRAME];
    unsigned char read_len;
    
    while(1){
      gettimeofday(&start, NULL);
      read_len = LinkNode_get_package(&link, read_buf, MAX_DATA_FRAME);
      int try = 0;
      while(LinkNode_send_package(&link, read_buf, read_len)){
        try++;
      }
      gettimeofday(&stop, NULL);
      msecs = (double)(stop.tv_usec - start.tv_usec) / 1000 + (double)(stop.tv_sec - start.tv_sec)*1000;
      ByteOK = (int)(read_len << 1) * 1000 / msecs;
  
      printf("Speed: %dB/s (%d times trying to feedback)\n", ByteOK, try);
  
      printf("Data got %d: ", read_len);
      for(int i = 0; i < read_len; i++){
        printf("%c", read_buf[i]);
      }
      printf("\n");
      ByteOK = 0;
      sleep(1);
    }
    LinkNode_deinit(&link);
    slog_deinit();
    return 0;
}

