#include <stdlib.h>
#include <unistd.h>
#include <stdio.h> 
#include <stdint.h>
#include <string.h>
#include <sys/time.h>

#include <host_link.h>
#include <slog.h>

int isRun;

int main(int argc, char **argv)
{
  unsigned int ByteOK, ByteError;
  ByteOK = ByteError = 0;
  isRun = 1;
  
  if(slog_init("host.log", 0, slog_all_flag)){
    printf("Unable to create log\n");
    return -1;
  }

  struct LinkHost_t link;
  if(LinkHost_init(&link)){
    fprintf(stderr, "Link init failed\n");
  }
  
  printf("Start host!\n");
  struct timeval start, stop;
  double msecs;
  const char *hello_str = "Hello World!!!";
  char buf[MAX_DATA_FRAME];
  int res;
  unsigned char data_size = strlen(hello_str);
  while(1){
    gettimeofday(&start, NULL);
    while(!LinkHost_send_package(&link, 32, (void*)hello_str, data_size)){
      printf("Failed to send. Try again!\n");
    }
    int try = 0;
    do{
      res = LinkHost_get_package(&link, 32, (void*)buf, MAX_DATA_FRAME);
      if(res < 0){
        try++;
        printf("Failed to get package. Try again! +%d\n", try);
      }else{
        printf("Finish to get package. %d time(s) trying\n", try);
        break;
      }
    }while(try < 5);
    gettimeofday(&stop, NULL);
    msecs = (double)(stop.tv_usec - start.tv_usec) / 1000 + (double)(stop.tv_sec - start.tv_sec)*1000;
    ByteOK = (data_size + res) * 1000 / msecs;

    buf[res] = 0;
    printf("Got %d data: %s\n", res, buf);

    unsigned int OK, Error, Speed;
    OK = ByteOK;
    Error = ByteError;
    ByteOK = ByteError = 0;
    Speed = OK + Error;

    printf("Speed: %dB/s, Error: %dB", Speed, Error);

    if(Speed > 0){
      float ratioOk = (OK) * 100.0/Speed;
      printf(", OK (%f%%)", ratioOk);
    }
    printf("\n");
    sleep(1);
  }
  printf("Bye\n");
  LinkHost_deinit(&link);
  slog_deinit();
  return 0;
}

