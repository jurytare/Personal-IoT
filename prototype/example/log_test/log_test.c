#include <slog.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
	if(argc > 1){
		int ret = slog_init("log_test.log", argv[1][0] != '0', slog_all_flag);
		printf("slog_init: %d\n", ret);
	}else{
		printf("Missing parameter\n"
			"./log_test 0 to save log to file\n"
			"./log_test 1 to show log to terminal\n");
		return -1;
	}
	SLOG_D("Hello world, this is %s", "debug");
	SLOG_I("Hello world, this is %s", "info");
	SLOG_W("Hello world, this is %s", "warning");
	SLOG_E("Hello world, this is %s", "error");
	SLOG_F("Hello world, this is %s", "fatal");
	return 0;
}
