#include <stdlib.h>
#include <unistd.h>
#include <stdio.h> 
#include <stdint.h>
#include <signal.h>
#include <sim_spi_module.h>

unsigned int ByteOK, ByteError;
unsigned char isRun;
unsigned char test_data;
FILE *f_log;

#define BIT(x) (0x01 << x)

int sim_spi_isr(struct simSPI_t *ss)
{
  if(ss->status & BIT(simSPI_status_RxHasData)){
    uint8_t dataComming;
    simSPI_data_get(ss, &dataComming);
    test_data++;
    if(dataComming == test_data){
      ByteOK++;
    }else{
      ByteError++;
      test_data = dataComming;
    }
    fprintf(f_log, "%.2x\n", test_data);
  }
  if(ss->status & BIT(simSPI_status_hasNotif)){
    fprintf(f_log, "Got a start signal\n");
  }
  return 0;
}

static void my_handler(int signo)
{
  isRun = 0;
}

int main(int argc, char **argv)
{

    ByteOK = ByteError = 0;
    isRun = 1;
    if(argc > 1)
      f_log = fopen(argv[1], "w");
    else{
      f_log = fopen("node.log", "w");
    }
    if(!f_log){
      printf("Unable to create log\n");
      return -1;
    }
    struct simSPI_t sim_spi;
    simSPI_init(&sim_spi, 0);
    simSPI_register_isr(&sim_spi, simSPI_mode_TRX_isr);
    simSPI_register_isr(&sim_spi, simSPI_mode_start_isr);
    simSPI_set_isr_callback(&sim_spi, &sim_spi_isr);
    simSPI_start(&sim_spi);

    if (signal(SIGINT, my_handler) == SIG_ERR)
    {
  	  printf("cannot create signal handler\n");
  	  exit(-1);
    }
      
    while(isRun){
      sleep(1);
      unsigned int OK, Error, Speed;
      OK = ByteOK;
      Error = ByteError;
      ByteOK = ByteError = 0;
      Speed = OK + Error;
      printf("Speed: %dB/s, Error: %dB", Speed, Error);

      if(Speed > 0){
        float ratioOk = (OK) * 100.0/Speed;
        printf(", OK (%f%%)", ratioOk);
      }
      printf("\n");
    }
    simSPI_deinit(&sim_spi);
    fclose(f_log);
    return 0;
}

