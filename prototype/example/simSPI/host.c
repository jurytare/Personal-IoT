#include <stdlib.h>
#include <unistd.h>
#include <stdio.h> 
#include <stdint.h>
#include <signal.h>
#include <sim_spi_module.h>

unsigned int ByteOK, ByteError;
unsigned char isRun;
unsigned char test_data;
FILE *f_log;

int sim_spi_isr(struct simSPI_t *ss)
{
  if(!(ss->status & (0x01 << simSPI_status_TxIsBusy))){
    simSPI_data_send(ss, test_data++); 
    ByteOK++;
    fprintf(f_log, "%.2x\n", test_data);
  }
  return 0;
}

static void my_handler(int signo)
{
  isRun = 0;
}

int main(int argc, char **argv)
{

    ByteOK = ByteError = 0;
    isRun = 1;
    f_log = fopen("host.log", "w");
    
    if(!f_log){
      printf("Unable to create log\n");
      return -1;
    }
    struct simSPI_t sim_spi;
    simSPI_init(&sim_spi, 1);
    simSPI_register_isr(&sim_spi, simSPI_mode_TRX_isr);
    simSPI_set_isr_callback(&sim_spi, &sim_spi_isr);
    simSPI_setMode(&sim_spi, simSPI_mode_TX);
    simSPI_start(&sim_spi);
    if (signal(SIGINT, my_handler) == SIG_ERR)
    {
  	  printf("cannot create signal handler\n");
  	  exit(-1);
    }
      
    while(isRun){
      sleep(1);
      simSPI_stop(&sim_spi);
      unsigned int OK, Error, Speed;
      OK = ByteOK;
      Error = ByteError;
      ByteOK = ByteError = 0;
      Speed = OK + Error;
      simSPI_start(&sim_spi);
      printf("Speed: %dB/s, Error: %dB", Speed, Error);

      if(Speed > 0){
        float ratioOk = (OK) * 100.0/Speed;
        printf(", OK (%f%%)", ratioOk);
      }
      printf("\n");
    }
    simSPI_deinit(&sim_spi);
    fclose(f_log);
    return 0;
}

