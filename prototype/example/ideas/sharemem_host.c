#include "sharemem_SPI.h"


int  shmClkid, shmDataid;
uint8_t *shmDatap, *shmClkp;
uint8_t clk, data;
uint8_t buffer[BUF_SIZE] = {0};
pid_t   pidList[10] = {0};
uint8_t pidListIndex = 0;
int newNodeIn = 0;
void sys_init(void);
void sys_denit(void);
sigset_t    blockSet;
void updatePidList(void);

static void sigHandler(int sig)
{
    if(sig == SIGUSR2)
    {
        printf("There is new node\n");
        newNodeIn = 1;
    }
}
int main(int argc, char**argv)
{
    clk = 0x0;
    
    sys_init();
    for(;;)
    {
        fgets(buffer, BUF_SIZE, stdin);
        int i, j;
        for(i = 0; buffer[i] != '\0' && buffer[i] != '\r' && buffer[i] != '\n'; i++)
        {
            clk ^= 0x1;
            *shmClkp = clk;
            *shmDatap = buffer[i];
            if(newNodeIn)
                updatePidList();
            for(j = 0; j < pidListIndex; j++)
                if(kill(pidList[j], SIGUSR1) == -1)
                    exit(-1);
            usleep(100);
            if(buffer[i] == 'q')
                exit(0);        
        }
    }
    sys_denit();
    exit(0);
}
void updatePidList(void)
{
    int i;
    char*buf;
    size_t buf_size = 0;
    ssize_t line_size = 0;
    FILE *fd = fopen(PROC_FILE, "r");
    for(i = 0; i < pidListIndex; i++)
        pidList[i] = 0;
    
    pidListIndex = 0;

    line_size = getline(&buf, &buf_size, fd);
    while(line_size >=0)
    {
        line_size = getline(&buf, &buf_size, fd);
        pid_t pid = (pid_t)atol(buf);
        pidList[pidListIndex++] = pid;    
    }
    // for(i = 0; i < pidListIndex;i ++)
    // {
    //     printf("nodePid[%d]=%ld\n", i, (long)pidList[i]);
    // }
    if(fclose(fd) == -1)
        exit(-1);
    newNodeIn = 0;
}
void sys_init(void)
{
    key_t clk_key = ftok(KEY_FILE, CLK_PROJ_ID);
    if(clk_key == -1)
        exit(-1);
    key_t data_key = ftok(KEY_FILE, DATA_PROJ_ID);
    if(data_key == -1)
        exit(-1);
    shmClkid = shmget(clk_key, 1, IPC_CREAT | OBJ_PERMS);
    if(shmClkid == -1)
        exit(ERROR_SHMGET);
    shmDataid = shmget(data_key, 1, IPC_CREAT | OBJ_PERMS);
    if(shmDataid == -1)
        exit(ERROR_SHMGET);
    
    shmDatap = shmat(shmDataid, NULL, 0);
    if(shmDatap == (void*) -1)
        exit(ERROR_SHMAT);
    shmClkp = shmat(shmClkid, NULL, 0);
    if(shmClkp == (void*) -1)
        exit(ERROR_SHMAT);

    *shmClkp = clk;

    sigemptyset(&blockSet);
    sigaddset(&blockSet, SIGUSR1);
    if(sigprocmask(SIG_BLOCK, &blockSet, NULL) == -1)
        exit(-1);

    int fd = open(PROC_FILE, O_CREAT | O_WRONLY |O_TRUNC, OBJ_PERMS  );
    char buf[10];
    sprintf(buf, "%ld", (long) getpid());
    int offset = lseek(fd, 0, SEEK_END);
    if( offset == -1)
        exit(-1);
    int res = write(fd, buf, strlen(buf));
    write(fd,"\n", 1);
    if(res == -1)
        exit(-1);
    
    if(signal(SIGUSR2, sigHandler) == SIG_ERR)
        exit(-1);
}

void sys_denit(void)
{
    if(shmdt(shmClkp) == -1)
        exit(ERROR_SHMDT);
    if(shmdt(shmDatap) == -1)
        exit(ERROR_SHMDT);
 
    if(shmctl(shmClkid, IPC_RMID, 0) == -1)
        exit(-1);   
    if(shmctl(shmDataid, IPC_RMID, 0) == -1)
        exit(-1);
}
