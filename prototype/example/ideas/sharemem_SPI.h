#define   _BSD_SOURCE

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <stdint.h>
#include <unistd.h>
#include  <pthread.h>
#include  <sys/time.h>
#include  <semaphore.h>
#include <signal.h>
#include <string.h>


#define     SHM_CLK_KEY     0x1235
#define     SHM_DATA_KEY    0x4231

#define     PROC_FILE       "procFile"
#define     KEY_FILE        "key-file"
#define     CLK_PROJ_ID     'c'
#define     DATA_PROJ_ID    'd'

#define     OBJ_PERMS   (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)

#define     HOST_WRITE_CLK_SEM   0
#define     NODE_READ_CLK_SEM    1

#define     ERROR_SHMGET        -2
#define     ERROR_SHMAT         -2
#define     ERROR_SHMDT         -2


#define     ERROR_THREAD_CREATE -3
#define     ERROR_SEM_DESTROY   -4
#define     BUF_SIZE            256