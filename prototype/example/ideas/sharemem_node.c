#include "sharemem_SPI.h"

int  shmClkid, shmDataid, s;
uint8_t *shmDatap, *shmClkp;
uint8_t clk, data;
pid_t hostPid = 0;
int isRun = 0;
static  sem_t   sem;

static void sigHandler(int sig)
{
    if(sig == SIGUSR1)
    {
        sem_post(&sem);
    }
}

void sys_init(void);
void sys_denit(void);
int main(int argc, char **argv)
{
    sys_init();
    clk = *shmClkp;
    for(;clk!= -1;)
    {
        uint8_t tmpClk = *shmClkp;
        if(tmpClk != clk)
        {
            clk = tmpClk;
            uint8_t buf = *shmDatap;
            if(buf == 'q')
                break;
            printf("clk=%d, data=%c\n",  (int) clk, (char) buf);
        }
        usleep(2);
        sem_wait(&sem);
    }
    sys_denit();
    printf("node stopped\n");
    exit(0);
}

void sys_init(void)
{
    key_t clk_key = ftok(KEY_FILE, CLK_PROJ_ID);
    if(clk_key == -1)
        exit(-1);
    key_t data_key = ftok(KEY_FILE, DATA_PROJ_ID);
    if(data_key == -1)
        exit(-1);

    shmClkid = shmget(clk_key, 0, 0);
    if(shmClkid == -1)
        exit(ERROR_SHMGET);
    shmDataid = shmget(data_key, 0, 0);
    if(shmDataid == -1)
        exit(ERROR_SHMGET);
    shmClkp = shmat(shmClkid, NULL, SHM_RDONLY);
    if(shmDatap == (void*) -1)
        exit(ERROR_SHMAT);   
    shmDatap = shmat(shmDataid, NULL, SHM_RDONLY);
    if(shmDatap == (void*) -1)
        exit(ERROR_SHMAT);

    sem_init(&sem, 0,0);


    if(signal(SIGUSR1, sigHandler) == SIG_ERR)
        exit(-1);

    int fd = open(PROC_FILE, O_CREAT | O_WRONLY, OBJ_PERMS  );
    char buf[10];
    char *line;
    size_t buf_size;
    sprintf(buf, "%ld", (long) getpid());
    int offset = lseek(fd, 0, SEEK_END);
    if( offset == -1)
        exit(-1);
    int res = write(fd, buf, strlen(buf));
    write(fd,"\n", 1);
    if(res == -1)
        exit(-1);
    if(close(fd) == -1)
        exit(-1);

    FILE *fp =fopen(PROC_FILE, "r");


    getline(&line,&buf_size, fp );
    hostPid = (pid_t)atol(line);
    printf("Host pid=%ld\n", (long)hostPid);
    if(hostPid == -1)
        exit(-1);
    if(fclose(fp) == -1)
        exit(-1);
    kill(hostPid, SIGUSR2);


}

void sys_denit(void)
{
    if(shmdt(shmClkp) == -1)
        exit(ERROR_SHMDT);
    if(shmdt(shmDatap) == -1)
        exit(ERROR_SHMDT);
    if(sem_destroy(&sem) == -1)
        exit(ERROR_SEM_DESTROY);
}